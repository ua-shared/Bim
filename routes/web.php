<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', function () {
    return view('welcome');
})->name("welcome");

Auth::routes();

Route::group([
    'middleware' => ['auth'],
    'prefix'=> 'user'
], function () {
    Route::get('/home', 'HomeController@index')->name('home');
    Route::get('/properties/pending', 'PropertyController@indexPending')->name('properties.pending');

    Route::delete('/properties/force-destroy/{property}', 'PropertyController@forceDestroy')
        ->where(['property' => '[0-9]+'])
        ->name('properties.forceDestroy');

    Route::delete('/properties/{property}/images/{image}', 'PropertyController@destroyImage')
        ->where([
            'property' => '[0-9]+',
            'image' => '[0-9]+',
            ])
        ->name('image.destroy');

    Route::get('/properties/{property}/images/{image}', 'PropertyController@setFeatureImage')
        ->where([
            'property' => '[0-9]+',
            'image' => '[0-9]+',
            ])
        ->name('image.set_featured');

    Route::get('/properties/{property}/images', 'PropertyController@addImage')
        ->where(['property' => '[0-9]+'])
        ->name('properties.add_image');

    Route::post('/properties/{property}/images', 'PropertyController@storeImage')
        ->where(['property' => '[0-9]+'])
        ->name('properties.store_image');

    Route::get('/properties/bookmarks', 'PropertyController@bookmarks_index')->name('bookmarks.index');
    Route::resource('/properties', 'PropertyController');
    Route::resource('/alertes', 'AlerteController');

});


// TEST
Route::get('/test', 'HomeController@test')->name('test');
