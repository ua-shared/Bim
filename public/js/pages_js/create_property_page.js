$(document).ready(function () {

    /*************************************************************
     *************************************************************
     * FUNCTIONS
     * ************************************************************
     * ************************************************************/
    function loadQuartiers(callback) {
        var ville = $('#ville').find(':selected').val();
        $.get(
            'https://bj-decoupage-territorial.herokuapp.com/api/v1/towns/' + ville + '/neighborhoods',
            '',
            function (data) {
                //var options = '<option value="" selected="">Choisissez un quartier</option>';
                var newOptions = [];
                newOptions.push(new Option('Choisissez un quartier', '', true, false));

                for (var i in data.neighborhoods) {
                    //options += '<option value="' + data.neighborhoods[i].name + '">' + data.neighborhoods[i].name + '</option>'
                    newOptions.push(new Option(data.neighborhoods[i].name, data.neighborhoods[i].name, false, false));
                }
                //$('#quartier').html().trigger('change');
                $('#quartier').html(newOptions).trigger('change');
            }
        ).always(callback);
    }

    function initmap() {
        // set up the map
        mymap = new L.Map('my_map');
        L.control.scale().addTo(mymap);

        // create the tile layer with correct attribution
        var osmUrl = 'http://{s}.tile.openstreetmap.org/{z}/{x}/{y}.png';
        var osmAttrib = 'Map data © <a href="http://openstreetmap.org">OpenStreetMap</a> contributors';
        var osm = new L.TileLayer(osmUrl, {minZoom: 5, maxZoom: 20, attribution: osmAttrib});


        mymap.setView(new L.LatLng(6.3866, 2.3778), 10);
        mymap.addLayer(osm);
        return mymap;
    }

    function onMapClick(e) {
        popup.setLatLng(e.latlng)
            .setContent("Vous avez sélectionné les coordonnées " + e.latlng.toString())
            .openOn(mymap);

        $('#lat').val(e.latlng.lat.toFixed(6));
        $('#lng').val(e.latlng.lng.toFixed(6));
    }

    /*
     * VALIDATORS
     */
    function checkBoxChecked(field) {
        return $('#' + field)["0"].checked;
    }

    function select2FieldValidator(field) {
        var selected = $('#' + field).val();
        selected = $.trim(selected);
        if (selected.length > 0) return true;
        return false;
    }

    function niceSelectFieldValidator(field) {
        var selected = $('#' + field + ' div.nice-select .option.selected').attr('data-value');
        selected = $.trim(selected);
        if (selected.length > 0) return true;
        return false;
    }

    /*
     * FEEDBACK
     */
    function displayFeedback(state, field, msg) {
        var fdb_span = $('#' + field + ' + span.feedback_span');
        if (state) {
            fdb_span.addClass('valid-feedback').removeClass('invalid-feedback');
            $('#' + field).removeClass('is-invalid');
        }
        else {
            fdb_span.addClass('invalid-feedback').removeClass('valid-feedback');
            $('#' + field).addClass('is-invalid');
        }
        fdb_span.html(msg).css('display', 'inline-block');
    }

    function removeFeedback(field) {
        $('#' + field + ' + span.feedback_span').css('display', 'none');
        $('#' + field).removeClass('is-invalid');
    }

    /*
    * EVENT HANDLERS
    * */
    function certifCheckboxChange() {
        if (!checkBoxChecked('certif')) {
            displayFeedback(false, 'lng', 'Vous devez cochez la déclaration de certification.');
            return false;
        }
        else {
            removeFeedback('lng')
        }
    }

    function addImgBtnClicked(e) {
        e.preventDefault();
        $('#img_others_fields').append(
            '<div class="mb-2">' +
            '<input type="file" class="" name="img_others_files[]">' +
            '</div>');
        if ($('input[name="img_others_files[]"]').length === 5) {
            $('#add_img_btn').attr('disabled', 'disabled');
            $('#add_img_btn').after("<p class='invalid-feedback feedback_span' role='alert'>" +
                "Vous ne pouvez plus ajouter d'autres images</p>").hide();
            $('#add_img_btn ~ p.feedback_span').css('display', 'inline-block');

        }
    }

    function createPropertyFormSubmit() {
        if (!checkBoxChecked('certif')) {
            displayFeedback(false, 'lng', 'Vous devez cochez la déclaration de certification.');
            return false;
        }
        if (!select2FieldValidator('ville')) {
            displayFeedback(false, 'ville', 'Vous devez selectionner une ville.');
            return false;
        }
        if (!select2FieldValidator('quartier')) {
            displayFeedback(false, 'quartier', 'Précisez le quartier.');
            return false;
        }
    }



    /*************************************************************
     *************************************************************
     * LOGIC
     * ************************************************************
     * ************************************************************/
    // Magnific Popup
    $('.card-columns').magnificPopup({
        delegate: 'a.image-link',
        type: 'image',
        gallery: {enabled: true}
    });

    // Delete an image
    $('a.form-delete').click(function (e) {
        e.preventDefault();
        let href = $(this).attr('href')
        $("form[action='" + href + "'").submit()
    })

    // Tooltip
    $('[data-toggle="tooltip"]').tooltip()

    /*******************************
     * INITIALIZING SELECT2 FIELDS
     * ******************************/
    $('#tags').select2({
        tags: true,
        createTag: function (params) {
            var term = $.trim(params.term);
            if (term === ''
                || !(/^[0-9a-zA-Z][0-9a-zA-Z-_]{1,99}[0-9a-zA-Z]$/.test(term))) {
                return null;
            }
            return {
                id: term,
                text: term,
                newTag: true // add additional parameters
            }
        }
    });

    $('#ville').select2();

    $('#quartier').select2({
        tags: true,
        createTag: function (params) {
            var term = $.trim(params.term);
            if (term === ''
                || !(/^[0-9a-zA-Z][0-9a-zA-Z-\\s]{1,99}[0-9a-zA-Z]$/.test(term))) {
                return null;
            }
            return {
                id: term.toUpperCase(),
                text: term.toUpperCase(),
                newTag: true // add additional parameters
            }
        }
    });

    var quartier = $('#quartier').find(':selected').val();
    loadQuartiers(function () {
        $('#quartier').val(quartier);
        $('#quartier').trigger('change');
    });

    $('#ville').change(function () {
        var ville = $('#ville').find(':selected').val();
        ville = $.trim(ville);
        if (ville.length == 0) return;
        // Je récupère les quartiers de la ville selectionnée
        loadQuartiers();
    });

    // Ajout des champs d'images
    $('#add_img_btn').click(addImgBtnClicked);

    // On update property form submit button click
    $('#create_property_form').submit(createPropertyFormSubmit);

    // On certif checkbox change
    $('#certif').change(certifCheckboxChange);


    /*
    * Ajout du map pour la localisation de la proprieté
    * */
    var lat = $('#lat').val();
    var lng = $('#lng').val();
    var current_Latlng = new L.LatLng(lat, lng);
    var mymap = initmap();
    var popup = L.popup({
        className: "mypopup"
    });

    mymap.on('click', onMapClick);




    /*
     * Images
     */
    $('.img_input_div').on('click', function (e) {
        $(e.target).siblings('input.img_input').click();
    });

    $('.img_input').change(function (e) {
        var input = e.target;
        var preview = $(input).siblings('div.img_preview');

        if (input.files && input.files[0]) {
            var file = input.files[0];
            var reader = new FileReader();
            reader.readAsDataURL(file);
            reader.onload = function (e) {
                preview.css('background-image', 'url(' + reader.result + ')').addClass('hasImage');
            }
        }
    });

    $('#add_img_btn').click(function (e) {
        e.preventDefault();
        var first = $('.hidden_img_input:first');
        if (first.length > 0) {
            first.removeClass('hidden_img_input').css('display', 'inline-block');
            $('#add_img_btn').show();
            $('#add_img_btn ~ p.feedback_span').css('display', 'none');

            var next = $('.hidden_img_input:first');
            if (next.length == 0) {
                $('#add_img_btn').hide();
                $('#add_img_btn ~ p.feedback_span').css('display', 'inline-block');
            }
        }
        first.children('input').click();
    });

    $('.delete_img_input_btn').click(function (e) {
        e.preventDefault();
        $(e.target).parent().parent().css('display', 'none').addClass('hidden_img_input')
            .removeClass('hasImage');
        $(e.target).parent().siblings('div.img_preview').css('background-image', '');
        $(e.target).parent().siblings('input.img_input').val('');

        var first = $('.hidden_img_input:first');
        if (first.length > 0) {
            $('#add_img_btn').show();
            $('#add_img_btn ~ p.feedback_span').css('display', 'none');
        }
    });

});
