$(document).ready(function () {

    // On create property form submit button click
    $('#create_property_form').submit(function (e) {

    });


    /*
     * Images
     */
    $('.img_input_div').on('click', function (e) {
        $(e.target).siblings('input.img_input').click();
    });
    $('.img_input').change(function (e) {
        var input = e.target;
        var preview = $(input).siblings('div.img_preview');

        if (input.files && input.files[0]) {
            var file = input.files[0];
            var reader = new FileReader();
            reader.readAsDataURL(file);
            reader.onload = function () {
                preview.css('background-image', 'url(' + reader.result + ')').addClass('hasImage');
            }
            /*if ((file.size / (1024 * 1024)) > 2) {
                $(e.target).siblings('span.feedback_span')
                    .html("Veuillez choisir une image dont la taille est inférieur à 2 Mo")
                    .css('display', 'inline-block');
            } else e.target.siblings('span.feedback_span').html('');*/
        }
    });

    $('#add_img_btn').click(function (e) {
        e.preventDefault();
        var first = $('.hidden_img_input:first');
        if (first.length > 0) {
            first.removeClass('hidden_img_input').css('display', 'inline-block');
            $('#add_img_btn').show();
            $('#add_img_btn ~ p.feedback_span').css('display', 'none');

            var next = $('.hidden_img_input:first');
            if (next.length == 0) {
                $('#add_img_btn').hide();
                $('#add_img_btn ~ p.feedback_span').css('display', 'inline-block');
            }
        }
        first.children('input').click();
    });

    $('.delete_img_input_btn').click(function (e) {
        e.preventDefault();
        $(e.target).parent().parent().css('display', 'none').addClass('hidden_img_input')
            .removeClass('hasImage');
        $(e.target).parent().siblings('div.img_preview').css('background-image', '');
        $(e.target).parent().siblings('input.img_input').val('');

        var first = $('.hidden_img_input:first');
        if (first.length > 0) {
            $('#add_img_btn').show();
            $('#add_img_btn ~ p.feedback_span').css('display', 'none');
        }
    });

});
