$(document).ready(function () {

    var located = false;
    $('#signup_form').mouseenter(function () {
        if(!located && checkBoxChecked('accept_locate')) {
            located = true;
            $('#accept_locate').trigger('change');
        }
    });

    // Registration form submit button
    $('#signup_form').submit(function (e) {
            if (!registrationFormValidator()) {
                displayFeedback(false, 'lgt', 'Tous les champs du formulaire ne sont pas valides. Revérifiez les champs.')
                return false;
            }
            else {
                removeFeedback('lgt')
            }
        }
    );

    function registrationFormValidator() {
        // Account Type
        if (niceSelectFieldValidator('account_type', ['personnal', 'enterprise']) === false) {
            accountTypeFeedback(false);
            return false;
        }

        // Email
        if (emailValidator($('#email').val()) === false) {
            emailFeedback(false);
            return false;
        }

        // Username
        if (usernameValidator($('#username').val()) === false) {
            usernameFeedback(false);
            return false
        }

        // Passwords
        if (checkPasswordsMatch() === false) return false

        // Name
        if (nameValidator($('#name').val()) === false) {
            nameFeedback(false);
            return false
        }

        // Firstname
        if ($('#firstname').attr('required') !== undefined) {
            if (!nameValidator($('#firstname').val())) {
                firstnameFeedback(false);
                return false
            }
        }

        // Tel
        if (telNumValidator($('#tel').val()) == false) {
            telNumFeedback(false);
            return false
        }

        // Address
        if ($('#address').val().length == 0) {
            addressFeedback(false);
            return false;
        }

        // Accept Locate
        if (checkBoxChecked('accept_locate') && ($('#lat').val().length == 0 || $('#lgt').val().length == 0)) {
            return false;
        }

        // Accept Contract
        if (checkBoxChecked('accept_contract') === false) {
            contractFeedback(false);
            return false;
        }

        return true;
    }

    /*
    * EVENTS
    * */
    $('#account_type').change(function () {
        var selected = niceSelectFieldValidator('account_type', ['personnal', 'enterprise']);
        accountTypeFeedback(selected);

        if (selected === "personnal") {
            $('#firstname').parent().css('display', 'block').attr('required', '');
        }
        else if (selected === 'enterprise') {
            $('#firstname').parent().css('display', 'none').removeAttr('required');
        }
    });

    $('#email').keyup(function () {
        emailFeedback(emailValidator($('#email').val()))
    });

    $('#username').keyup(function () {
        usernameFeedback(usernameValidator($('#username').val()));
    });

    $('#password, #password-confirm').keyup(function () {
        checkPasswordsMatch()
    });

    $('#name').keyup(function () {
        nameFeedback(nameValidator($('#name').val()));
    });

    $('#firstname').keyup(function () {
        firstnameFeedback(nameValidator($('#firstname').val()));
    });

    $('#tel').keyup(function () {
        telNumFeedback(telNumValidator($('#tel').val()));
    });

    $('#address').keyup(function () {
        console.log($('#address').val());

        addressFeedback(addressValidator($('#address').val()));
    });

    $('#accept_contract').change(function () {
        contractFeedback(checkBoxChecked('accept_contract'));
    });

    $('#accept_locate').change(function () {
        if (checkBoxChecked('accept_locate')) {
            displayCBFeedback(false, 'accept_locate', 'Patientez, localisation en cours . . .');
            locateUser();
            locateFeedback(true);
        } else {
            delocateUser();
            locateFeedback(false);
        }
    });

    /*
    * FEEDBACKS
    * */
    function accountTypeFeedback(state) {
        if (!$('#account_type + div.nice-select').attr('id')) {
            $('#account_type + div.nice-select').attr('id', 'account_type_nice_select')
        }
        if (state) {
            removeFeedback('account_type_nice_select');
            //displayFeedback(true, 'email', "Email valide.");
        } else {
            displayFeedback(false, 'account_type_nice_select', "&nbsp;&nbsp;(Choisissez un type de compte)");
        }
    }

    function emailFeedback(state) {
        if (state) {
            removeFeedback('email');
            //displayFeedback(true, 'email', "Email valide.");
        } else {
            displayFeedback(false, 'email', "L'email est invalide.");
        }
    }

    function usernameFeedback(state) {
        if (state) {
            removeFeedback('username');
            //displayFeedback(true, 'username', "Nom d'utilisateur valide.");
        } else {
            displayFeedback(false, 'username', "Le nom d'utilisateur ne peut contenir des caractères spéciaux. " +
                "Seul les caractères alphanumériques, '-' et '_' sont autorisés et doit contenir au moins 3 caractères.");
        }
    }

    function nameFeedback(state) {
        if (state) {
            removeFeedback('name');
        } else {
            displayFeedback(false, 'name', "Le nom ne peut contenir des caractères spéciaux. " +
                "Seul les lettres et '-' sont autorisés et doit contenir au moins 3 caractères.");
        }
    }

    function firstnameFeedback(state) {
        if (state) {
            removeFeedback('firstname');
            //displayFeedback(true, 'firstname', "Le prénom est valide.");
        } else {
            displayFeedback(false, 'firstname', "Le prénom ne peut contenir des caractères spéciaux. " +
                "Seul les lettres et '-' sont autorisés et doit contenir aiu moins 3 caractères.");
        }
    }

    function telNumFeedback(state) {
        if (state) {
            removeFeedback('tel');
            //displayFeedback(true, 'tel', "Le numero de téléphone est valide.");
        } else {
            displayFeedback(false, 'tel', "Le numero de téléphone est invalide.");
        }
    }

    function addressFeedback(state) {
        if (state) {
            removeFeedback('address')
        } else {
            displayFeedback(false, 'address', "Renseignez le champ addresse.");
        }
    }

    function locateFeedback(state) {
        if (state) {
            //removeCBFeedback('accept_locate');
        } else {
            displayCBFeedback(false, 'accept_locate', "Autorisez nous à utiliser votre localisation !");
        }
    }

    function contractFeedback(state) {
        if (state) {
            removeCBFeedback('accept_contract')
        } else {
            displayCBFeedback(false, 'accept_contract', "Vous devez accepter les conditions d'utilisation pour pouvoir vous inscrire.");
        }
    }


    /*
    * VALIDATORS
    * */
    function niceSelectFieldValidator(field, goodValues) {
        var selected = $('#account_type + div.nice-select .option.selected').attr('data-value');

        for (var key in goodValues) {
            if (goodValues[key] == selected) {
                return selected;
            }
        }

        return false;
    }

    function telNumValidator(tel) {
        tel = String(tel).toLowerCase();
        tel = $.trim(tel);
        var regex = /^(\+|\d)[0-9-\s]{7,20}$/;
        return regex.test(tel);
    }

    function usernameValidator(username) {
        username = String(username).toLowerCase();
        username = $.trim(username);
        var regex = /^[0-9a-zA-Z][\w-]{1,48}[0-9a-zA-Z]$/;
        return regex.test(username);
    }

    function nameValidator(name) {
        name = String(name).toLowerCase();
        name = $.trim(name);
        var regex = /^[0-9a-zA-Z][0-9a-zA-Z-\s]{1,48}[0-9a-zA-Z]$/;
        return regex.test(name) && name.length > 2;
    }

    function emailValidator(email) {
        email = String(email).toLowerCase();
        email = $.trim(email);
        var regex = /^(([^<>()[\]\\.,;:\s@\"]+(\.[^<>()[\]\\.,;:\s@\"]+)*)|(\".+\"))@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\])|(([a-zA-Z\-0-9]+\.)+[a-zA-Z]{2,}))$/;
        return regex.test(email);
    }

    function addressValidator(addr) {
        addr = $.trim(addr);
        return addr.length > 1;
    }

    function checkBoxChecked(field) {
        return $('#' + field)["0"].checked;
    }

    function checkPasswordsMatch() {
        var password = $("#password").val();
        var confirmPassword = $("#password-confirm").val();

        if (password != confirmPassword || password.length < 6) {
            displayFeedback(false, 'password-confirm', "Les mots de passe doivent concorder et contenir au moins 6 caractères.");
            return false;
        }
        else {
            removeFeedback('password-confirm');
            return true;
        }
    }

    /*
     * FEEDBACK
     */

    function displayFeedback(state, field, msg) {
        var fdb_span = $('#' + field + ' + span.feedback_span');
        if (state) {
            fdb_span.addClass('valid-feedback').removeClass('invalid-feedback');
            $('#' + field).removeClass('is-invalid');
        }
        else {
            fdb_span.addClass('invalid-feedback').removeClass('valid-feedback');
            $('#' + field).addClass('is-invalid');
        }
        fdb_span.html(msg).css('display', 'inline-block');
    }

    function displayCBFeedback(state, field, msg) {
        var fdb_span = $('#' + field).parent().next('span');
        if (state) {
            fdb_span.addClass('valid-feedback').removeClass('invalid-feedback');
            $('#' + field).removeClass('is-invalid');
        }
        else {
            fdb_span.addClass('invalid-feedback').removeClass('valid-feedback');
            $('#' + field).addClass('is-invalid');
        }
        fdb_span.html(msg).css('display', 'inline-block');
    }

    function removeFeedback(field) {
        $('#' + field + ' + span.feedback_span').css('display', 'none');
        $('#' + field).removeClass('is-invalid');
    }

    function removeCBFeedback(field) {
        $('#' + field).parent().next('span').css('display', 'none');
        $('#' + field).removeClass('invalid-feedback').removeClass('valid-feedbac');
        $('#' + field).removeClass('is-invalid');
    }

    /*
    * GETTING LOCATION
    * */

    function locateUser() {
        $('#signup_btn').attr('disabled', 'disabled');
        navigator.geolocation.getCurrentPosition(function(location) {
            $('#lat').val(location.coords.latitude);
            $('#lgt').val(location.coords.longitude);
            displayCBFeedback(true, 'accept_locate', 'Localisation terminée avec succès !');
        }, function (e) {
            console.log(e);
            displayCBFeedback(false, 'accept_locate', 'La localisation à echoué. ' +
                'Soyez connecté à internet puis décochez et recochez le bouton !');
        });
        $('#signup_btn').removeAttr('disabled');
    }

    function delocateUser() {
        $('#lat').val('');
        $('#lgt').val('');
    }

    /*function locateMe2() {
        var HELSINKI = [60.1708, 24.9375];
        var map = L.map('map_div');
        var tile_url = 'http://{s}.tile.osm.org/{z}/{x}/{y}.png';
        var layer = L.tileLayer(tile_url, {
            attribution: 'OSM'
        });
        map.addLayer(layer);
        map.setView(HELSINKI, 19);

        map.locate({setView: true, watch: true})
    .on('locationfound', function(e){
            console.log('lgt', e.longitude);
            console.log('lat', e.latitude);
            var marker = L.marker([e.latitude, e.longitude]).bindPopup('Your are here :)');
            var circle = L.circle([e.latitude, e.longitude], e.accuracy/2, {
                weight: 1,
                color: 'blue',
                fillColor: '#cacaca',
                fillOpacity: 0.2
            });
            map.addLayer(marker);
            map.addLayer(circle);
        })
            .on('locationerror', function(e){
                console.log(e);
                alert("Location access denied.");
            });
    }*/

})
;
