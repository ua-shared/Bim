$(document).ready(function () {

    function booleanRenderer(val) {
        if (val == 1 || val == true) return 'Oui';
        else return 'Non';
    }

    function subFormat(name1, value1, name2, value2) {
        return '<tr>' +
            '   <td class="beforeData"></td>' +
            '   <td>' + name1 + '</td>' +
            '   <td>' + value1 + '</td>' +
            '   <td class="betweenData">' + '</td>' +
            '   <td>' + name2 + '</td>' +
            '   <td>' + value2 + '</td>' +
            '   <td class="afterData"></td>' +
            '</tr>';
    }

    /* Formatting function for row details - modify as you need */
    function format(d) {
        // `d` is the original data object for the row
        return '<table cellpadding="5" cellspacing="0" border="0" style="padding-left:50px;">' +
            subFormat('Nombre de pièces : ', d.pieces, 'Nombre de garages : ', d.garages) +
            subFormat('Nombre de lits : ', d.lits, 'Piscine : ', booleanRenderer(d.piscine)) +
            subFormat('Nombre de douches : ', d.douches, 'Jardin : ', booleanRenderer(d.jardin)) +
            subFormat("Nombre d'étages : ", d.etages, 'Vue sur la mer : ', booleanRenderer(d.vue_sur_mer)) +
            '<tr>' +
            '   <td class="beforeData"></td>\n' +
            '   <td>Description&nbsp;:&nbsp;</td>' +
            '   <td colspan="5">' + d.desc + '</td>' +
            '</tr>' +
            '<tr>' +
            '   <td colspan="7">' +
            '   <a href="' + d.edit_url + '" class="btn btn-primary m-2">Modifier</a>' +
            '   <button class="btn btn-warning m-2"' +
            '       onclick=\'document.getElementById(\"deleteProperty' + d.id + '\").submit();\'>' +
            '      Supprimer</button>' +
            '   <button class="btn btn-warning m-2"' +
            '       onclick=\'document.getElementById(\"forceDeleteProperty' + d.id + '\").submit();\'>' +
            '       Supprimer Définitivement</button>' +
            '   </td>' +
            '</tr>' +
            '</table>';
    }

    var table = $('#propertiesList').DataTable({
        "ajax": "../../data/data.json",
        "columns": [
            {
                "className": 'details-control',
                "orderable": false,
                "data": null,
                "defaultContent": ''
            },
            {"data": "name"},
            {"data": "type_operation"},
            {"data": "type_propriete"},
            {"data": "ville"},
            {"data": "quartier"},
            {"data": "prix"},
            {"data": "superficie"},
            {"data": "created_at"},
        ],
        "order": [[1, 'asc']],
        "scrollX": true,
        "scrollY": "600px",
        "scrollCollapse": true,
    });

    // Add event listener for opening and closing details
    $('#propertiesList tbody').on('click', 'td.details-control', function () {
        var tr = $(this).closest('tr');
        var row = table.row(tr);

        if (row.child.isShown()) {
            // This row is already open - close it
            row.child.hide();
            tr.removeClass('shown');
        }
        else {
            // Open this row
            row.child(format(row.data())).show();
            tr.addClass('shown');
        }
    });

});
