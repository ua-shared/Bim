<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreatePropertiesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('properties', function (Blueprint $table) {
            //$table->engine = 'InnoDB';
            $table->increments('id');
            $table->enum('state', ['pending', 'published', 'unavailable'])->nullable();
            $table->unsignedInteger('user_id');
            $table->foreign('user_id')->references('id')->on('users')->onDelete('cascade');
            $table->string('name');
            $table->string('slug')->unique();
            $table->string('ville');
            $table->string('quartier');
            $table->double('lat')->nullable();
            $table->double('lng')->nullable();
            $table->double('prix');
            $table->double('superficie');
            $table->enum('type_operation', ['Louer', 'Vendre']);
            $table->string('type_propriete');
            $table->integer('pieces');
            $table->integer('lits');
            $table->integer('douches');
            $table->integer('garages');
            $table->integer('etages');
            $table->boolean('piscine');
            $table->boolean('jardin');
            $table->boolean('vue_sur_mer');
            $table->text('desc');
            $table->timestamps();
            $table->softDeletes();
        });
    }

    /**
     *
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('properties');
    }
}
