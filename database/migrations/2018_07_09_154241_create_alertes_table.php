<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateAlertesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('alertes', function (Blueprint $table) {
            //$table->engine = 'InnoDB';
            $table->increments('id');
            $table->enum('state', ['on', 'off'])->default('on');
            $table->unsignedInteger('user_id');
            $table->foreign('user_id')->references('id')->on('users')->onDelete('cascade');
            $table->string('name');
            $table->string('slug')->unique();
            $table->string('ville')->nullable();
            $table->string('quartier')->nullable();
            $table->double('lat')->nullable();
            $table->double('lng')->nullable();
            $table->double('rayon')->nullable();
            $table->double('prix_min');
            $table->double('prix_max')->nullable();
            $table->double('superficie_min');
            $table->double('superficie_max')->nullable();
            $table->enum('type_operation', ['louer', 'vendre']);
            $table->string('type_propriete');
            $table->integer('nb_pieces');
            $table->integer('nb_lits');
            $table->integer('nb_douches');
            $table->integer('nb_garages');
            $table->integer('nb_etages');
            $table->boolean('piscine');
            $table->boolean('jardin');
            $table->boolean('vue_sur_mer');
            $table->string('desc');
            $table->dateTime('echeance_alerte')->nullable();
            $table->timestamps();
            $table->softDeletes();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('alertes');
    }
}
