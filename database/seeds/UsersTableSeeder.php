<?php

use App\Models\User;
use Illuminate\Database\Seeder;

class UsersTableSeeder extends Seeder {
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run() {
        User::create([
            'name' => 'User01',
            'email' => 'user01@test.bj',
            'role' => 'user',
            'password' => bcrypt('aaaaaa'),
            'firstname' => 'Firstname01',
            'username' => 'username01',
            'account_type' => 'personnal',
            'tel' => '+229 00 00 00 01',
            'address' => 'Benin Littoral Hoeuyiho',
            'lat' => '6.3702',
            'lng' => '2.3906',
            'settings' => '',
        ]);

        User::create([
            'name' => 'Enterprise02',
            'email' => 'user02@test.bj',
            'role' => 'user',
            'password' => bcrypt('aaaaaa'),
            'username' => 'username02',
            'account_type' => 'enterprise',
            'tel' => '+229 00 00 00 02',
            'address' => 'Benin Littoral Kindonou',
            'lat' => '6.3890',
            'lng' => '2.3615',
            'settings' => '',
        ]);

        User::create([
            'name' => 'Admin03',
            'email' => 'user03@test.bj',
            'role' => 'admin',
            'password' => bcrypt('aaaaaa'),
            'firstname' => 'Firstname03',
            'username' => 'username03',
            'account_type' => 'personnal',
            'tel' => '+229 00 00 00 03',
            'address' => 'Benin Littoral Dantokpa',
            'lat' => '6.3742',
            'lng' => '2.4302',
            'settings' => '',
        ]);
    }
}
