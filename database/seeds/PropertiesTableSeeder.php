<?php

use App\Models\Image;
use App\Models\Property;
use App\Models\Tag;
use Illuminate\Database\Seeder;

class PropertiesTableSeeder extends Seeder {
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run() {
        $property = Property::create([
            'user_id' => 1,
            'name' => 'Prop01',
            'ville' => 'COTONOU',
            'quartier' => 'FIFADJI',
            'lat' => 6.429409,
            'lng' => 2.32275,
            'prix' => 500000,
            'superficie' => 13,
            'type_operation' => 'Louer',
            'type_propriete' => 'Appartements',
            'pieces' => 5,
            'lits' => 2,
            'douches' => 1,
            'garages' => 2,
            'etages' => 1,
            'piscine' => 1,
            'jardin' => 0,
            'vue_sur_mer' => 0,
            'desc' => 'Desc',
            'created_at' => '2018-07-11 15:43:11',
            'updated_at' => '2018-07-12 00:37:14',
        ]);

        Image::create([
            'property_id' => $property->id,
            'name' => 'properties/' . $property->user_id . '/Fpbs346xMlseRqZcJV4BlmCxfgG8essDTpOvg9nI.jpeg',
            'ftd' => 1,
        ]);

        Image::create([
            'property_id' => $property->id,
            'name' => 'properties/' . $property->user_id . '/FYfW3S2tTjRFE5o76nxtg0UYRKqDfSAOvbXpjHjN.jpeg',
        ]);

        Image::create([
            'property_id' => $property->id,
            'name' => 'properties/' . $property->user_id . '/JnT5JEEPoqrADfQGNpn9t7btXgHiZfUMTVHpsVz0.jpeg'
        ]);

        $tag = Tag::firstOrCreate(['name' => 'tag1']);
        $property->tags()->attach($tag->id);

        $tag = Tag::firstOrCreate(['name' => 'tag2']);
        $property->tags()->attach($tag->id);

        $tag = Tag::firstOrCreate(['name' => 'tag3']);
        $property->tags()->attach($tag->id);

        $property = Property::create([
            'user_id' => 1,
            'name' => 'Prop02',
            'ville' => 'KANDI',
            'quartier' => 'AKOITCHAOU',
            'lat' => 6.429409,
            'lng' => 2.32275,
            'prix' => 120000000,
            'superficie' => 55,
            'type_operation' => 'Vendre',
            'type_propriete' => 'Maisons-Villas',
            'pieces' => 10,
            'lits' => 7,
            'douches' => 5,
            'garages' => 4,
            'etages' => 3,
            'piscine' => 1,
            'jardin' => 1,
            'vue_sur_mer' => 0,
            'desc' => 'Desc',
            'created_at' => '2018-07-11 15:43:11',
            'updated_at' => '2018-07-12 00:37:14',
        ]);

        Image::create([
            'property_id' => $property->id,
            'name' => 'properties/' . $property->user_id . '/LDuKV0ZhhBc6Wvszasp5HoXTcnD39zEm2eh7Uw8U.jpeg',
            'ftd' => 1,
        ]);

        Image::create([
            'property_id' => $property->id,
            'name' => 'properties/' . $property->user_id . '/M0e0HGXDLzKkh4dgrfFLNfTKZT68RvezebVootuJ.jpeg',
        ]);

        Image::create([
            'property_id' => $property->id,
            'name' => 'properties/' . $property->user_id . '/MHFWnKFZh6Kxmhec3hDdR0vBvYVMGeuApRM44JqP.jpeg',
        ]);

        Image::create([
            'property_id' => $property->id,
            'name' => 'properties/' . $property->user_id . '/p10DRQ40ONJ1ld7mxnvj1LazTlyUYZdJxiZklROa.jpeg'
        ]);

        $tag = Tag::firstOrCreate(['name' => 'tag4']);
        $property->tags()->attach($tag->id);

        $tag = Tag::firstOrCreate(['name' => 'tag3']);
        $property->tags()->attach($tag->id);

        Property::create([
            'user_id' => 1,
            'name' => 'Prop03',
            'ville' => 'ABOMEY-CALAVI',
            'quartier' => 'TOGOUDO',
            'lat' => 6.429409,
            'lng' => 2.32275,
            'prix' => 10000,
            'superficie' => 4,
            'type_operation' => 'Louer',
            'type_propriete' => 'Residences etudiantes',
            'pieces' => 1,
            'lits' => 0,
            'douches' => 1,
            'garages' => 0,
            'etages' => 0,
            'piscine' => 0,
            'jardin' => 0,
            'vue_sur_mer' => 0,
            'desc' => 'Desc',
            'created_at' => '2018-07-11 15:43:11',
            'updated_at' => '2018-07-12 00:37:14',
        ]);

        Image::create([
            'property_id' => $property->id,
            'name' => 'properties/' . $property->user_id . 'q3vQvjdt1mW2nei6issvVlOiqsfvZw8gEOevVfA9.jpeg',
            'ftd' => 1,
        ]);

        Image::create([
            'property_id' => $property->id,
            'name' => 'properties/' . $property->user_id . '/Yrz1z6yrVUVEsuEnlsRjyLFbRw9xltRper6LO6lY.jpeg',
        ]);
    }
}
