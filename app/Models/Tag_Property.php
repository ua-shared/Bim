<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Tag_Property extends Model {
    protected $fillable = [
        'property_id',
        'tag_id'
    ];
}
