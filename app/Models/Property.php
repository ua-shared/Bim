<?php

namespace App\Models;

use App\Models\Tag;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\Builder;
use Illuminate\Database\Eloquent\SoftDeletes;

class Property extends Model
{

    use SoftDeletes;

    protected $fillable = [
        'user_id',
        'name',
        'slug',
        'ville',
        'quartier',
        'lng',
        'lat',
        'prix',
        'superficie',
        'type_operation',
        'type_propriete',
        'pieces',
        'lits',
        'douches',
        'garages',
        'etages',
        'piscine',
        'jardin',
        'vue_sur_mer',
        'desc',
    ];

    /**
     * The attributes that should be mutated to dates.
     *
     * @var array
     */
    protected $dates = ['deleted_at'];

    public function tags()
    {
        return $this->belongsToMany('App\Models\Tag');
    }

    public function images() {
        return $this->hasMany('App\Models\Image');
    }

    public function nonFtdImages() {
        return $this->hasMany('App\Models\NonFtdImage');
    }

    public function ftdImage() {
        return $this->hasOne('App\Models\FtdImage');
    }

    // Scopes
    protected static function boot()
    {
        parent::boot();

        static::addGlobalScope('forCurrentUser', function (Builder $builder) {
            $builder->where('user_id', auth()->id());
        });
    }

    public function scopePublished($query)
    {
        return $query->where('state', 'published');
    }

    public function scopePending($query)
    {
        return $query->where('state', 'pending');
    }
}
