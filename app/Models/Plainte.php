<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Plainte extends Model
{
    protected $fillable = [
        'userp_id',
        'property_id',
        'objet',
        'desc',
    ];
}
