<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class UtilityData extends Model
{
    protected $fillable = ['name', 'value'];

    public $timestamps = false;
}
