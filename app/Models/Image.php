<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Image extends Model {
    protected $fillable = ['property_id', 'name', 'ftd'];

    public $timestamps = false;

    protected $table = 'images';
}
