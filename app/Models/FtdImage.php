<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Builder;

class FtdImage extends Image
{
    protected static function boot()
    {
        parent::boot();

        static::addGlobalScope('ftd', function (Builder $builder) {
            $builder->where('ftd', 1);
        });
    }

    public function __construct(array $attributes = []) {
        parent::__construct($attributes);

        // Attributes default values
        $this->ftd = 1;
    }

    public function unset() {
        $this->ftd = 0;
        $this->save();
    }
}
