<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Alerte extends Model
{

    protected $fillable = [
        'user_id',
        'name',
        'slug',
        'ville',
        'quartier',
        'lng',
        'lat',
        'prix_min',
        'prix_max',
        'superficie_min',
        'superficie_max',
        'type_operation',
        'type_proprerty',
        'nb_pieces',
        'nb_lits',
        'nb_douches',
        'nb_garages',
        'nb_etages',
        'piscine',
        'jardin',
        'vue_sur_mer',
        'desc',
        'echeance_alerte',
    ];
}
