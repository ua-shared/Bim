<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Builder;

class NonFtdImage extends Image
{
    protected static function boot()
    {
        parent::boot();

        static::addGlobalScope('nonFtd', function (Builder $builder) {
            $builder->where('ftd', 0);
        });
    }

    public function __construct(array $attributes = []) {
        parent::__construct($attributes);

        // Attributes default values
        $this->ftd = 0;
    }

    public function setAsFtd() {
        $this->ftd = 1;
        $this->save();
    }
}
