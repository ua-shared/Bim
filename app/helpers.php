<?php

if (!function_exists('currentRoute')) {
    function currentRoute(...$routes)
    {
        foreach($routes as $route) {
            if(request()->url() == route($route)) {
                return ' active currentRoute';
            }
        }
    }
}

if (!function_exists('is_selected')) {
    function is_selected($selected = null, $value = null)
    {
        if(!isset($selected) || is_null($selected)) return '';
        else {
            if (!isset($value)) return 'selected';
            else {
                if ($selected == $value) return 'selected';
                else return '';
            }
        }
    }
}

if (!function_exists('booleanRenderer')) {
    function booleanRenderer($val)
    {
        if($val == 1 || $val == true) return 'Oui';
        else return 'Non';
    }
}

if (!function_exists('formatDate')) {
    function formatDate($date_string) {
        $date = new \DateTime($date_string);
        return $date->format("d/m/Y H:i");
    }
}
