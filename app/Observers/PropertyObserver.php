<?php

namespace App\Observers;

use App\Models\Property;

class PropertyObserver
{

    public function creating(Property $property)
    {
        $property->slug = $property->user_id . '-' . str_slug($property->name, '-') . '-' . time();
        !isset($property->piscine) ? $property->piscine = 0 : '';
        !isset($property->jardin) ? $property->jardin = 0 : '';
        !isset($property->vue_sur_mer) ? $property->vue_sur_mer = 0 : '';
        if(auth()->check() && auth()->user()->role === 'admin') $property->state = 'published';
        else $property->state = 'pending';
    }
}
