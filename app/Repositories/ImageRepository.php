<?php

namespace App\Repositories;

use App\Models\Image;
use App\Models\FtdImage;
use App\Models\NonFtdImage;
use App\Models\Property;
use Illuminate\Support\Facades\Session;
use Illuminate\Support\Facades\Storage;
use Intervention\Image\Facades\Image as InterventionImage;

class ImageRepository {

    public function store(&$validated, Property $property) {
        $img = new FtdImage($this->addImage($validated['img_vdt_file'], $property)->toArray());
        $img->save();

        // Save the other images
        $this->addImages($validated['img_others_files'], $property);
    }


    /**
     * @param          $files
     * @param Property $property
     * @return
     */
    public function addImages($files, Property $property) {
        foreach ($files as $file) {
            $this->addImage($file, $property);
        }
    }

    public function addImage($file, Property $property) {
        $image = false;
        if ($property->images()->count() < 6) {
            try {
                // Create thumb
                $thumb = InterventionImage::make($file)->widen(500);
                // Save image
                $path = Storage::disk('properties')->put(auth()->id(), $file);
                // Save thumb
                Storage::disk('thumbs')->put($path, $thumb->encode());
                // Save in base
                $image = NonFtdImage::create([
                    'name' => $path,
                    'property_id' => $property->id,
                ]);
            } catch (Exception $e) {
                Session::flash('warning', "Une erreur est survenue lors de la sauvegarde des images : $e");
            }
        }
        return $image;
    }

    public function deletePropertyImages(Property $property) {
        foreach ($property->images as $image) {
            $this->deleteImage($image);
        }
    }

    public function deleteImage(Image $image) {
        try {
            Storage::disk('properties')->delete($image->name);
            Storage::disk('thumbs')->delete($image->name);
        } finally {
            $image->delete();
        }
    }

}
