<?php

namespace App\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;
use Illuminate\Validation\Rule;

class PropertyUpdateRequest extends FormRequest {
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize() {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules() {
        return [
            "certif" => 'bail|required|accepted',
            "type_operation" => [
                'required',
                Rule::in(['Louer', 'Vendre']),
            ],
            "name" => "required|string|max:255",
            "ville" => "required|string|max:255",
            "quartier" => "required|string|max:255",
            "prix" => "required|numeric",
            "superficie" => "required|numeric",
            "type_propriete" => "required|string|max:255",
            "pieces" => "required|numeric",
            "lits" => "required|numeric",
            "douches" => "required|numeric",
            "garages" => "required|numeric",
            "etages" => "required|numeric",
            "piscine" => "nullable|digits_between:1,1",
            "jardin" => "nullable|digits_between:1,1",
            "vue_sur_mer" => "nullable|digits_between:1,1",
            "lat" => "required|numeric",
            "lng" => "required|numeric",
            "desc" => "nullable|string",
            "tags" => "nullable|array",
            "tags.*" => [
                'string',
                'max:255',
                'regex:/^[0-9a-zA-Z][0-9a-zA-Z-\\s]{1,99}[0-9a-zA-Z]$/',
            ],
        ];
    }
}
