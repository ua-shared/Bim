<?php

namespace App\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;
use Illuminate\Validation\Rule;

class PropertyStoreRequest extends FormRequest {
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize() {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules() {
        return [
            "certif" => 'bail|required|accepted',
            "type_operation" => [
                'required',
                Rule::in(['Louer', 'Vendre']),
            ],
            "name" => "required|string|max:255",
            "ville" => "required|string|max:255",
            "quartier" => "required|string|max:255",
            "prix" => "required|numeric",
            "superficie" => "required|numeric",
            "type_propriete" => "required|string|max:255",
            "pieces" => "required|numeric",
            "lits" => "required|numeric",
            "douches" => "required|numeric",
            "garages" => "required|numeric",
            "etages" => "required|numeric",
            "piscine" => "nullable|digits_between:1,1",
            "jardin" => "nullable|digits_between:1,1",
            "vue_sur_mer" => "nullable|digits_between:1,1",
            "lat" => "required|numeric",
            "lng" => "required|numeric",
            "desc" => "nullable|string",
            "tags" => "nullable|array",
            "tags.*" => [
                'string',
                'max:255',
                'regex:/^[0-9a-zA-Z][0-9a-zA-Z-\\s]{1,99}[0-9a-zA-Z]$/',
            ],
            "img_vdt_file" => "required|mimes:jpeg,png|max:2000",
            "img_others_files" => "required|min:1|max:5",
            "img_others_files.*" => "required|mimes:jpeg,png|max:2000",
        ];
    }

    public function messages() {
        return [
            'img_vdt_file.mimes' => 'Seules les images au format .jpg et .png sont autorisées.',
            'img_vdt_file.max' => 'Vous avez dépassé la taille maximale (de 2 Mo) autorisée par image.',
            'img_others_files.*.mimes' => 'Seules les images au format .jpg et .png sont autorisées.',
            'img_others_files.*.max' => 'Vous avez dépassé la taille maximale (de 2 Mo) autorisée par image.',
            'img_others_files.min' => "Vous devez choisir au minimum une image en plus de l'image vedette.",
            'img_others_files.max' => "Vous devez choisir au maximum 5 images en plus de l'image vedette.",
        ];
    }
}
