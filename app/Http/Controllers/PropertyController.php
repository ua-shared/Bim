<?php

namespace App\Http\Controllers;

use App\Http\Requests\PropertyStoreRequest;
use App\Http\Requests\PropertyUpdateRequest;
use App\Models\Image;
use App\Models\FtdImage;
use App\Models\NonFtdImage;
use App\Models\Property;
use App\Models\Tag;
use App\Models\UtilityData;
use App\Repositories\ImageRepository;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Storage;
use Illuminate\Support\Facades\Validator;

class PropertyController extends Controller {

    protected $imager;

    /**
     * Create a new ImageController instance.
     *
     * @param  \App\Repositories\ImageRepository $imager
     */
    public function __construct(ImageRepository $imager) {
        $this->imager = $imager;
    }

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index() {
        $properties = Property::published()->get();
        $data = [];
        $ids = [];
        foreach ($properties as $property) {
            $ids[] = $property->id;
            $pr = $property->toArray();
            $pr['created_at'] = formatDate($property->created_at);
            $pr['edit_url'] = route('properties.edit', $property->id);
            /*$pr['destroy_url'] = route('properties.destroy', $property->id);
            $pr['force_destroy_url'] = route('properties.forceDestroy', $property->id);*/
            $data[] = $pr;
        }
        $data = ['data' => $data];
        Storage::disk('data')->put('data.json', json_encode($data));

        return view('properties.index_published', compact('ids'));
    }

    public function indexPending() {
        $properties = Property::pending()->get();
        $data = [];
        $ids = [];
        foreach ($properties as $property) {
            $ids[] = $property->id;
            $pr = $property->toArray();
            $pr['created_at'] = formatDate($property->created_at);
            $pr['edit_url'] = route('properties.edit', $property->id);
            /*$pr['destroy_url'] = route('properties.destroy', $property->id);*/
            $data[] = $pr;
        }
        $data = ['data' => $data];
        Storage::disk('data')->put('data.json', json_encode($data));

        return view('properties.index_pending', compact('ids'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create() {
        $villes = UtilityData::where('name', 'villes')->first()->value;
        $villes = json_decode($villes);
        return view('properties.create', compact('villes'));
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request $request
     * @return \Illuminate\Http\Response
     */
    public function store(PropertyStoreRequest $request) {
        $validated = $request->validated();
        $validated['user_id'] = auth()->id();
        $validated['state'] = 'pending';
        $property = Property::create($validated);

        !isset($validated['tags']) ? $validated['tags'] = [] : '';
        foreach ($validated['tags'] as $name) {
            $tag = Tag::firstOrCreate(compact('name'));
            $property->tags()->attach($tag->id);
        }

        $this->imager->store($validated, $property);

        return redirect()->route('properties.pending');
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Models\Property $property
     * @return \Illuminate\Http\Response
     */
    public function show(Property $property) {
        return view('properties.show', $property);
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Models\Property $property
     * @return \Illuminate\Http\Response
     */
    public function edit(Property $property) {
        if (!empty($property)) {
            $villes = UtilityData::where('name', 'villes')->first()->value;
            $villes = json_decode($villes);
            $types_propriete = UtilityData::where('name', 'types_propriete')->first()->value;
            $types_propriete = json_decode($types_propriete);
            return view('properties.edit', compact('property', 'villes', 'types_propriete'));
        }
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request $request
     * @param  \App\Models\Property     $property
     * @return \Illuminate\Http\Response
     */
    public function update(PropertyUpdateRequest $request, Property $property) {
        $validated = $request->validated();
        $validated['state'] = 'pending';

        !isset($validated['piscine']) ? $validated['piscine'] = 0 : '';
        !isset($validated['jardin']) ? $validated['jardin'] = 0 : '';
        !isset($validated['vue_sur_mer']) ? $validated['vue_sur_mer'] = 0 : '';
        $property->update($validated);
        $property->tags()->detach();

        !isset($validated['tags']) ? $validated['tags'] = [] : '';

        foreach ($validated['tags'] as $name) {
            $tag = Tag::firstOrCreate(compact('name'));
            $property->tags()->attach($tag->id);
        }
        debug('Ok');
        return redirect()->route('properties.pending')->with('success', 'Les données ont été mises à jour avec succès !');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Models\Property $property
     * @return void
     * @throws \Exception
     */
    public function destroy(Property $property) {
        $property->delete();
        return redirect()->back();
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Models\Property $property
     * @return void
     * @throws \Exception
     */
    public function forceDestroy(Property $property) {
        $this->imager->deletePropertyImages($property);
        $property->forceDelete();
        return redirect()->back();
    }



    /***********************************************
     ********* BOOKMARKS ************************
     ***********************************************/
    public function bookmarks_index() {

    }




    /***********************************************
     ********* IMAGES ************************
     **********************************************
     * @param Property $property
     * @param Image    $image
     * @return \Illuminate\Http\RedirectResponse
     * @throws \Exception
     */
    public function destroyImage(Property $property, Image $image) {
        if ($property->id === $image->property_id
            && auth()->id() === $property->user_id
            && $property->images()->count() > 2) {
            $this->imager->deleteImage($image);
        } else {
            return redirect()->back()->with('error', "Vous n'etes pas autorisé à supprimer cette photo !");
        }
        return redirect()->back()->with('success', 'La photo a été supprimée avec succès !');
    }

    /**
     * @param Property $property
     * @param Image    $image
     * @return \Illuminate\Http\RedirectResponse
     */
    public function setFeatureImage(Property $property, NonFtdImage $image) {
        if ($property->id === $image->property_id
            && auth()->id() === $property->user_id) {
            $property->ftdImage->unset();
            $image->setAsFtd();
        }
        return redirect()->back()->with('success', 'La photo vedette a été correctement mise à jour !');
    }

    /**
     * @param Property $property
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\View\View
     */
    public function addImage(Property $property) {
        $nbImgs = $property->images()->count();
        if ($property->images()->count() < 5) {
            return view('properties.add_image', [
                'nbImgs' => 5 - $nbImgs,
                'property' => $property
            ]);
        }
    }

    /**
     * @param Request  $request
     * @param Property $property
     * @return \Illuminate\Http\RedirectResponse
     */
    public function storeImage(Request $request, Property $property) {
        $max = 5 - $property->images()->count();

        $rules = [
            "img_others_files" => "required|array|min:1|max:$max",
            "img_others_files.*" => "required|mimes:jpeg,png|max:20000",
        ];

        $validator = Validator::make($request->all(), $rules);

        if ($validator->fails()) {
            return redirect()->back()
                ->with('error', 'Seules les images de taille inférieure à 2 Mo et au format .jpg et .png  sont autorisées.');
        }

        $validated = $request->all();
        $this->imager->addImages($validated['img_others_files'], $property);

        return redirect()->route('properties.edit', $property);
    }

}
