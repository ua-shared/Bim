<?php

namespace App\Http\Controllers\Auth;

use App\Models\User;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Hash;
use Illuminate\Support\Facades\Validator;
use Illuminate\Foundation\Auth\RegistersUsers;

class RegisterController extends Controller
{
    /*
    |--------------------------------------------------------------------------
    | Register Controller
    |--------------------------------------------------------------------------
    |
    | This controller handles the registration of new users as well as their
    | validation and creation. By default this controller uses a trait to
    | provide this functionality without requiring any additional code.
    |
    */

    use RegistersUsers;

    /**
     * Where to redirect users after registration.
     *
     * @var string
     */
    protected $redirectTo = 'user/home';

    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('guest');
    }

    /**
     * Get a validator for an incoming registration request.
     *
     * @param  array  $data
     * @return \Illuminate\Contracts\Validation\Validator
     */
    protected function validator(array $data)
    {

        $rules = [
            'signup_btn' => 'bail|required|string',
            'accept_contract' => 'bail|required|string',
            'name' => 'required|string|max:255',
            'username' => 'required|string|max:255|unique:users',
            'email' => 'required|string|email|max:255|unique:users',
            'password' => 'required|string|min:6|confirmed',
            'tel' => 'required|string|max:50',
            'role' => 'string|max:10',
            'account_type' => 'required|string|max:15',
            'address' => 'required|string|max:2000',
        ];

        if ($data['account_type'] == 'personnal') {
            $rules['firstname'] = 'required|string|max:255';
        } elseif ($data['account_type'] == 'enterprise') {
            $rules['firstname'] = 'string|max:255';
        }

        if (isset($data['accept_locate']) && $data['accept_locate'] == 'accept_locate') {
            $rules['lat'] = 'required|string|max:255';
            $rules['lng'] = 'required|string|max:255';
        }

        return Validator::make($data, $rules);
    }

    /**
     * Create a new user instance after a valid registration.
     *
     * @param  array  $data
     * @return \App\Models\User
     */
    protected function create(array $data)
    {
        $user = [
            'name' => $data['name'],
            'username' => $data['username'],
            'firstname' => $data['firstname'],
            'email' => $data['email'],
            'password' => Hash::make($data['password']),
            'tel' => $data['tel'],
            'role' => 'user',
            'account_type' => $data['account_type'],
            'address' => $data['address'],
            'address' => $data['address'],
            'address' => $data['address'],
            'lat' => $data['lat'],
            'lng' => $data['lng'],
        ];

        return User::create($user);
    }
}
