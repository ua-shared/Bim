<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreatePropertiesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('properties', function (Blueprint $table) {
            $table->increments('id');
            $table->boolean('is_property')->default(true);
            $table->integer('user_id')->unsigned();
            $table->foreign('user_id')->references('id')->on('users')->onDelete('cascade');
            $table->string('name');
            $table->string('slug')->unique();
            $table->string('ville');
            $table->string('quartier');
            $table->double('lgt');
            $table->double('lat');
            $table->double('prix');
            $table->double('prix_max');
            $table->double('superficie');
            $table->double('superficie_max');
            $table->string('type_operation');
            $table->string('type_prop');
            $table->integer('nb_pieces');
            $table->integer('nb_lits');
            $table->integer('nb_douches');
            $table->integer('nb_garages');
            $table->integer('nb_etages');
            $table->boolean('piscine');
            $table->boolean('jardin');
            $table->boolean('vue_sur_mer');
            $table->string('desc');
            $table->dateTime('echeance_alerte');
            $table->string('img_vdt');
            $table->text('imgs');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('properties');
    }
}
