@extends('layouts.menu_app')

@section('page_specific_scripts')
    <script src="{{ asset("vendor/datatables/src/jquery.dataTables.js") }}"></script>
    <script src="{{ asset("js/pages_js/properties_index_page.js") }}"></script>
@endsection

@section('page_specific_styles')
    <link rel="stylesheet" href="{{ asset('vendor/datatables/src/jquery.dataTables.css') }}"/>
@endsection

@section('menu_app_content')
    <div class="row justify-content-center">
        @include('layouts.includes.flashMessages')
        <div class="col">

            <table id="propertiesList" class="display" width="100%">
                <thead>
                <tr role="row">
                    <th>Options</th>
                    <th>Code</th>
                    <th>Nom</th>
                    <th>Opération</th>
                    <th>Ville</th>
                    <th>Quartier</th>
                    <th>Prix</th>
                    <th>Superficie</th>
                    <th>Pièces</th>
                    <th>Lits</th>
                    <th>Douches</th>
                    <th>Garages</th>
                    <th>Etages</th>
                    <th>Piscine</th>
                    <th>Jardin</th>
                    <th>Vue&nbsp;sur&nbsp;la&nbsp;mer</th>
                    <th>Description</th>
                    <th>Date de création</th>
                </tr>
                </thead>

                <tbody>
                @foreach($properties as $pr)
                    <tr role="row" class="odd">
                        <td>
                            <a href="{{ route('properties.edit', $pr) }}">Modifier</a><br>
                            <a href="javascript:void(0)" id="deleteProperty"
                               onclick="document.getElementById('deleteProperty{{ $pr->id }}').submit();">
                                Supprimer</a>
                            <a href="javascript:void(0)" id="deleteProperty"
                               onclick="document.getElementById('forceDeleteProperty{{ $pr->id }}').submit();">
                                Supprimer&nbsp;Def</a>

                            <form id="deleteProperty{{ $pr->id }}"
                                  action="{{ route('properties.destroy', $pr) }}" method="POST">
                                @csrf
                                @method('DELETE')
                            </form>
                            <form id="forceDeleteProperty{{ $pr->id }}"
                                  action="{{ route('properties.forceDestroy', $pr) }}" method="POST">
                                @csrf
                                @method('DELETE')
                            </form>
                        </td>
                        <td>{{ $pr->slug }}</td>
                        <td>{{ $pr->name }}</td>
                        <td>{{ $pr->type_operation }}</td>
                        <td>{{ $pr->ville }}</td>
                        <td>{{ $pr->quartier }}</td>
                        <td>{{ $pr->prix }}</td>
                    <td>{{ $pr->superficie }}</td>
                    <td>{{ $pr->pieces }}</td>
                    <td>{{ $pr->lits }}</td>
                    <td>{{ $pr->douches }}</td>
                    <td>{{ $pr->garages }}</td>
                    <td>{{ $pr->etages }}</td>
                    <td>{{ booleanRenderer($pr->piscine) }}</td>
                    <td>{{ booleanRenderer($pr->jardin) }}</td>
                    <td>{{ booleanRenderer($pr->vue_sur_mer) }}</td>
                    <td>{{ $pr->desc }}</td>
                    <td>{{ $pr->created_at }}</td>
                    </tr>
                @endforeach
                </tbody>
                <tfoot>
                <tr role="row">
                    <th>Options</th>
                    <th>Code</th>
                    <th>Nom</th>
                    <th>Opération</th>
                    <th>Ville</th>
                    <th>Quartier</th>
                    <th>Prix</th>
                    <th>Superficie</th>
                    <th>Pièces</th>
                    <th>Lits</th>
                    <th>Douches</th>
                    <th>Garages</th>
                    <th>Etages</th>
                    <th>Piscine</th>
                    <th>Jardin</th>
                    <th>Vue&nbsp;sur&nbsp;la&nbsp;mer</th>
                    <th>Description</th>
                    <th>Date de création</th>
                </tr>
                </tfoot>

            </table>

        </div>
    </div>
@endsection
