<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Estate extends Model {
    protected $fillable = [
        'is_property',
        'user_id',
        'name',
        'slug',
        'ville',
        'quartier',
        'lng',
        'lat',
        'prix',
        'prix_max',
        'superficie',
        'superficie_max',
        'type_operation',
        'type_prop',
        'nb_pieces',
        'nb_lits',
        'nb_douches',
        'nb_garages',
        'nb_etages',
        'piscine',
        'jardin',
        'vue_sur_mer',
        'desc',
        'echeance_alerte',
        'img_vdt',
        'imgs'
    ];
}
