<div class="row">
    <div class="col mb-2">
        Facturation
    </div>
</div>
<div class="row">
    <div class="col">
        <div class="form-inline">
            <label class="sr-only" for="inlineFormInputName2">Montant</label>
            <input type="text" class="form-control mb-2 mr-sm-2" id="inlineFormInputName2"
                   placeholder="Jane Doe">

            &nbsp;FCFA tous les&nbsp;

            <input type="number" class="form-control mb-2 mr-sm-2" id="inlineFormInputName3"
                   placeholder="1">

            <label class="sr-only" for="inlineFormInputGroupUsername2">Username</label>
            <select name="periode" id="periode" class="form-control nice_select"
                    style="min-width: 200px;">
                <option value="jours">Jours</option>
                <option value="semaines">Semaines</option>
                <option value="mois">Mois</option>
                <option value="ans">Ans</option>
            </select>
        </div>
    </div>
</div>
