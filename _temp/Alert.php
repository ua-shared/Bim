<?php

namespace App\Models;

class Alert extends Estate {

    protected $fillable = [
        'is_property',
        'user_id',
        'name',
        'slug',
        'ville',
        'quartier',
        'lng',
        'lat',
        'prix',
        'prix_max',
        'superficie',
        'superficie_max',
        'type_operation',
        'type_prop',
        'nb_pieces',
        'nb_lits',
        'nb_douches',
        'nb_garages',
        'nb_etages',
        'piscine',
        'jardin',
        'vue_sur_mer',
        'desc',
        'echeance_alerte',
        'img_vdt',
        'imgs'
    ];

    protected $table = 'estates';

    public function __construct(array $attributes = []) {
        parent::__construct($attributes);

        // Attributes default values
        $this->is_property = false;
        $this->img_vdt = null;
        $this->imgs = null;

        $this->prix_min = $this->getPrixMin();
        $this->superficie_min =$this->getSuperficieMin();
    }

    protected static function boot() {
        parent::boot();

        // Global scope for excluding properties
        static::addGlobalScope('isnot_property', function (Builder $builder) {
            $builder->where('is_property', false);
        });
    }

    private function getPrixMin() {
        return $this->prix;
    }

    private function getSuperficieMin() {
        return $this->superficie;
    }
}
