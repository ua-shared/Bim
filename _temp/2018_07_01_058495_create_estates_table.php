<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateEstatesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('estates', function (Blueprint $table) {
            $table->increments('id');
            $table->boolean('is_property')->default(true);
            $table->integer('user_id')->unsigned();
            $table->foreign('user_id')->references('id')->on('users')->onDelete('cascade');
            $table->string('name');
            $table->string('slug')->unique();
            $table->string('ville');
            $table->string('quartier');
            $table->double('lgt')->nullable();
            $table->double('lat')->nullable();
            $table->double('prix');
            $table->double('prix_max')->nullable();
            $table->double('superficie');
            $table->double('superficie_max')->nullable();
            $table->string('type_operation');
            $table->string('type_prop');
            $table->integer('nb_pieces');
            $table->integer('nb_lits');
            $table->integer('nb_douches');
            $table->integer('nb_garages');
            $table->integer('nb_etages');
            $table->boolean('piscine');
            $table->boolean('jardin');
            $table->boolean('vue_sur_mer');
            $table->string('desc');
            $table->dateTime('echeance_alerte')->nullable();
            $table->string('img_vdt')->nullable();
            $table->text('imgs')->nullable();
            $table->timestamps();
            $table->softDeletes();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('estates');
    }
}
