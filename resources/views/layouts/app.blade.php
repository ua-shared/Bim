<!DOCTYPE html>
<html lang="{{ str_replace('_', '-', app()->getLocale()) }}">
<head>
    @include("layouts.includes.head")
</head>
<body id="body">
<!-- Preloader -->
<div id="preloader">
    <div class="south-load"></div>
</div>
@include("layouts.includes.nav")
@yield('app_content')
@include("layouts.includes.footer")
<!-- Scrips -->
@include("layouts.includes.footer-scripts")
</body>
</html>
