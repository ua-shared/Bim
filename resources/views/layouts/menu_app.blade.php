@extends('layouts.app')

@section('app_content')
    <div class="top_margin bottom_margin page_min_height" id="user_home_page">
        <div class="my_container">
            <div class="row">
                <div class="col-sm-3 col-md-3">
                    <div id="menu">
                        <button class="accordion menu_item">Proprietés</button>
                        <div class="panel">
                            <ul class="list-group">
                                <a href="{{ route('properties.create') }}" class="{{ currentRoute('properties.create') }}"><li class="list-group-item">Publier une annonce</li></a>
                                <a href="{{ route('properties.index') }}" class="{{ currentRoute('properties.index') }}"><li class="list-group-item">Annonces publiées</li></a>
                                <a href="{{ route('properties.pending') }}" class="{{ currentRoute('properties.pending') }}"><li class="list-group-item">Annonces en attente</li></a>
                                <a href="{{ route('bookmarks.index') }}" class="{{ currentRoute('bookmarks.index') }}"><li class="list-group-item">Mes favoris</li></a>
                            </ul>
                        </div>

                        <button class="accordion menu_item">Alertes</button>
                        <div class="panel">
                            <ul class="list-group">
                                <a href="{{ route('alertes.create') }}" class="{{ currentRoute('alertes.create') }}"><li class="list-group-item">Créer une alerte</li></a>
                                <a href="{{ route('alertes.index') }}" class="{{ currentRoute('alertes.index') }}"><li class="list-group-item">Mes alertes</li></a>
                            </ul>
                        </div>

                        <button class="accordion menu_item">Corbeille</button>
                    </div>
                </div>

                <div class="col-sm-9 col-md-9">
                    @yield('menu_app_content')
                </div>
            </div>
        </div>
    </div>
@endsection
