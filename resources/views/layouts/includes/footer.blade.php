<!-- ##### Footer Area Start ##### -->
<footer class="footer-area section-padding-100-0 bg-img gradient-background-overlay"
        style="background-image: url({{ asset('img/bg-img/cta.jpg') }});">
    <!-- Main Footer Area -->
    <div class="main-footer-area">
        <div class="container">
            <div class="row">

                <!-- Single Footer Widget -->
                <div class="col-12 col-sm-6 col-xl-3">
                    <div class="footer-widget-area mb-100">
                        <!-- Widget Title -->
                        <div class="widget-title">
                            <h6>&Agrave; propos de nous</h6>
                        </div>

                        <img src="{{ asset('img/bg-img/footer.jpg') }}" alt="">
                        <div class="footer-logo my-4">
                            <img src="{{ asset('img/bim-logo.png') }}" alt="">
                        </div>
                        <p>Cette plateforme réunit en une seule place, la majeur partie des biens immobiliers
                            disponibles au Bénin.
                            Nous offrons la possibilités de publier des annonces et de faire des recherches poussées
                            basées sur différents critères
                            et sur les coordonnées géographiques.</p>
                    </div>
                </div>

                <!-- Single Footer Widget -->
                <div class="col-12 col-sm-6 col-xl-3">
                    <div class="footer-widget-area mb-100">
                        <!-- Widget Title -->
                        <div class="widget-title">
                            <h6>Adresse</h6>
                        </div>
                        <!-- Address -->
                        <div class="address">
                            <h6><img src="{{ asset('img/icons/phone-call.png') }}" alt=""> +229 62 16 67 00</h6>
                            <h6><img src="{{ asset('img/icons/envelope.png') }}" alt=""> ulrichnan@gmail.com</h6>
                            <h6><img src="{{ asset('img/icons/location.png') }}" alt=""> Rue 492 Cocotomey - Abomey-Calavi (BENIN)</h6>
                        </div>
                    </div>
                </div>

                <!-- Single Footer Widget -->
                <div class="col-12 col-sm-6 col-xl-3">
                    <div class="footer-widget-area mb-100">
                        <!-- Widget Title -->
                        <div class="widget-title">
                            <h6>Liens utiles</h6>
                        </div>
                        <!-- Nav -->
                        <ul class="useful-links-nav d-flex align-items-center">
                            <li><a href="#">Acceuil</a></li>
                            <li><a href="#">&Agrave; propos</a></li>
                            <li><a href="#">Nos services</a></li>
                            <li><a href="#">Les proprietés</a></li>
                            <li><a href="#">Témoignages</a></li>
                            <li><a href="#">Nous contacter</a></li>
                            <li><a href="#">Foire Aux Questions</a></li>
                        </ul>
                    </div>
                </div>

                <!-- Single Footer Widget -->
                <div class="col-12 col-sm-6 col-xl-3">
                    <div class="footer-widget-area mb-100">
                        <!-- Widget Title -->
                        <div class="widget-title">
                            <h6>Nos meilleures offres</h6>
                        </div>
                        <!-- Featured Properties Slides -->
                        <div class="featured-properties-slides owl-carousel">
                            <!-- Single Slide -->
                            <div class="single-featured-properties-slide">
                                <a href="#"><img src="{{ asset('img/bg-img/fea-product.jpg') }}" alt=""></a>
                            </div>
                            <!-- Single Slide -->
                            <div class="single-featured-properties-slide">
                                <a href="#"><img src="{{ asset('img/bg-img/fea-product.jpg') }}" alt=""></a>
                            </div>
                            <!-- Single Slide -->
                            <div class="single-featured-properties-slide">
                                <a href="#"><img src="{{ asset('img/bg-img/fea-product.jpg') }}" alt=""></a>
                            </div>
                        </div>
                    </div>
                </div>

            </div>
        </div>
    </div>

    <!-- Copywrite Text -->
    <div class="copywrite-text d-flex align-items-center justify-content-center">
        <p><!-- Link back to Colorlib can't be removed. Template is licensed under CC BY 3.0. -->
            Copyright &copy;<script>document.write(new Date().getFullYear());</script>
            All rights reserved | This template is made with <i class="fa fa-heart-o" aria-hidden="true"></i> by <a
                href="https://colorlib.com" target="_blank">Colorlib</a>
            <!-- Link back to Colorlib can't be removed. Template is licensed under CC BY 3.0. -->
    </div>
</footer>
<!-- ##### Footer Area End ##### -->
