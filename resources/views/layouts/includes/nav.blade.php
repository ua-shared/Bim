<!-- ##### Header Area Start ##### -->
<header class="header-area">

    <!-- Main Header Area -->
    <div class="main-header-area" id="stickyHeader">
        <div class="classy-nav-container breakpoint-off">
            <!-- Classy Menu -->
            <nav class="classy-navbar justify-content-between" id="southNav">

                <!-- Logo -->
                <a class="nav-brand" href="{{ route('welcome') }}"><img src="{{ asset('img/bim-logo.png') }}"
                                                                        alt=""></a>

                <!-- Navbar Toggler -->
                <div class="classy-navbar-toggler">
                    <span class="navbarToggler"><span></span><span></span><span></span></span>
                </div>

                <!-- Menu -->
                <div class="classy-menu">

                    <!-- close btn -->
                    <div class="classycloseIcon">
                        <div class="cross-wrap"><span class="top"></span><span class="bottom"></span></div>
                    </div>

                    <!-- Nav Start -->
                    <div class="classynav">
                        <ul>
                            <li><a href="{{ route('home') }}">Acceuil</a></li>
                            <li><a href="{{ route('properties.index') }}">Proprietés</a></li>
                            <li><a href="#">A propos</a></li>
                            <li><a href="#">Contact</a></li>
                            @guest
                                @if(request()->url() == route('login'))
                                    <li><a href="{{ route('register') }}">
                                            <button type="button" class="btn south-btn my_color_01">
                                                Inscription
                                            </button>
                                        </a></li>
                                @else(request()->url() == route('welcome'))
                                    <li><a href="{{ route('login') }}">
                                            <button type="button" class="btn south-btn my_color_01">
                                                Connexion
                                            </button>
                                        </a></li>
                                @endif
                            @else
                                <li><a href="#">
                                        {{ Auth::user()->name }}
                                        <span class="caret"></span>
                                    </a>

                                    <ul class="dropdown">
                                        <li><a href="{{ route('home') }}">
                                                Espace membre
                                            </a></li>
                                        <li><a href="{{ route('logout') }}"
                                            onclick="event.preventDefault();
                                            document.getElementById('logout-form').submit();">Se deconnecter</a></li>
                                    </ul>
                                </li>
                                <form id="logout-form" action="{{ route('logout') }}" method="POST"
                                      style="display: none;">
                                    @csrf
                                </form>
                            @endguest
                        </ul>

                        <!-- Search Form -->
                        <div class="south-search-form">
                            <form action="#" method="post">
                                <input type="search" name="search" id="search" placeholder="Search Anything ...">
                                <button type="submit"><i class="fa fa-search" aria-hidden="true"></i></button>
                            </form>
                        </div>

                        <!-- Search Button -->
                        <a href="#" class="searchbtn"><i class="fa" aria-hidden="true"></i></a>
                    </div>
                    <!-- Nav End -->
                </div>
            </nav>
        </div>
    </div>
</header>
<!-- ##### Header Area End ##### -->
