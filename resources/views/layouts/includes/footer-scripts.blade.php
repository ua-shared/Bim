<!-- jQuery (Necessary for All JavaScript Plugins) -->
@section('jquery')
{{--<script src="{{ asset("vendor/jquery/jquery-2.2.4.min.js") }}"></script>--}}
<script src="{{ asset("vendor/jquery/jquery-3.3.1.js") }}"></script>
@show
<!-- Popper js -->
<script src="{{ asset("js/popper.min.js") }}"></script>
<!-- Bootstrap js -->
<script src="{{ asset("js/bootstrap.js") }}"></script>
<!-- Plugins js -->
<script src="{{ asset("js/plugins_updated.js") }}"></script>
<script src="{{ asset("js/classy-nav.min.js") }}"></script>
<script src="{{ asset("js/jquery-ui.min.js") }}"></script>
<!-- Active js -->
<script src="{{ asset("vendor/select2/select2.js") }}"></script>
<script src="{{ asset("js/active.js") }}"></script>
@yield('page_specific_scripts')
<script src="{{ asset("js/app.js") }}"></script>
@yield('page_specific_scripts_2')
