<meta charset="UTF-8">
<meta name="description" content="">
<meta http-equiv="X-UA-Compatible" content="IE=edge">
<meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
<!-- The above 4 meta tags *must* come first in the head; any other head content must come *after* these tags -->

<meta name="author" content="Chanil">

<title>{{ config('app.name', 'Laravel') }}</title>

<!-- CSRF Token -->
<meta name="csrf-token" content="{{ csrf_token() }}">

<!-- Favicon  -->
<link rel="icon" href="{{ asset("img/core-img/favicon.ico") }}">

<!-- Style CSS -->
<link rel="stylesheet" href="{{ asset("css/south-template-style.css") }}">
<link rel="stylesheet" href="{{ asset("vendor/select2/select2.css") }}">
@yield('page_specific_styles')
<link rel="stylesheet" href="{{ asset("css/app.css") }}">
@yield('page_specific_styles_2')
