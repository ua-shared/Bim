<div id="{{ isset($group_id) ? $group_id : '' }}">
    @foreach($options as $value => $label)
        <div class="custom-control custom-radio mb-3">
            <input type="radio" class="custom-control-input"
                   id="{{ $name }}_{{ $value }}"
                   value="{{ $value }}"
                   name="{{ $name }}"
                   {{ isset($checked) && $checked == $value ? 'checked' : '' }}
                   required>
            <label class="custom-control-label" for="{{ $name }}_{{ $value }}">{{ $label }}</label>
        </div>
    @endforeach

    <span class="invalid-feedback feedback_span" role="alert">
            <strong>
                @if ($errors->has($name) && !isset($hideErrors))
                    {{ $errors->first($name) }}
                @endif
            </strong>
    </span>
</div>
