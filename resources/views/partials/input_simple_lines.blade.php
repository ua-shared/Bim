<div class="form-group">
    <label for="{{ $name }}">{{ $title }}</label>
    <input id="{{ isset($id) ? $id : $name }}" name="{{ $name }}"
           type="{{ $type }}" class="form-control{{ $errors->has($name) ? ' is-invalid' : '' }}"
           value="{{ old($name, isset($value) ? $value : '') }}" {{ $required ? 'required' : ''}}
        {!! !empty($others) ? $others : '' !!}>

    <span class="invalid-feedback feedback_span" role="alert">
            <strong>
                @if ($errors->has($name) && !isset($hideErrors))
                    {{ $errors->first($name) }}
                @endif
            </strong>
    </span>
</div>
