<div class="custom-file {{ isset($div_class) ? $div_class : '' }}">
    <input type="file" class="custom-file-input{{ $errors->has($name) ? ' is-invalid' : '' }}"
           id="{{ isset($id) ? $id : $name }}" name="{{ $name }}" {{ $required ? 'required' : ''}}
        {!! !empty($others) ? $others : '' !!}>
    <label class="custom-file-label" for="{{ $name }}">{{ $title }}</label>

    <span class="invalid-feedback feedback_span" role="alert">
            <strong>
                @if ($errors->has($name) && !isset($hideErrors))
                    {{ $errors->first($name) }}
                @endif
            </strong>
    </span>
</div>
