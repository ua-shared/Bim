<div class="form-group">
    <label for="{{ $name }}">{{ $title }}</label>
    <textarea id="{{ isset($id) ? $id : $name }}"  name="{{ $name }}"
              style="height: {{ isset($height) ? $height : "100px" }};"
           class="form-control{{ $errors->has($name) ? ' is-invalid' : '' }}"
           {{ isset($required) && $required ? 'required' : ''}}
        {!! !empty($others) ? $others : '' !!}>{{ old($name, isset($value) ? $value : '') }}</textarea>

    <span class="invalid-feedback feedback_span" role="alert">
            <strong>
                @if ($errors->has($name) && !isset($hideErrors))
                    {{ $errors->first($name) }}
                @endif
            </strong>
    </span>
</div>
