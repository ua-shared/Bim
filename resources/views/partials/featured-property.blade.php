<!-- Single Featured Property -->
<div class="col-12 col-md-6 col-xl-4">
    <div class="single-featured-property mb-50 wow fadeInUp" data-wow-delay="100ms">
        <!-- Property Thumbnail -->
        <div class="property-thumb">
            <img src="img/bg-img/feature1.jpg" alt="">

            <div class="tag">
                <span>{{ $property->op_type }}</span>
            </div>
            <div class="list-price">
                <p>{{ $property->price }} F CFA</p>
            </div>
        </div>
        <!-- Property Content -->
        <div class="property-content">
            <h5>{{ $property->name }}</h5>
            <p class="location"><img src="img/icons/location.png" alt="">{{ $property->addr }}
            <p>{{ $property->desc }}</p>
            <div class="property-meta-data d-flex align-items-end justify-content-between">
                <div class="new-tag">
                    <img src="img/icons/new.png" alt="">
                </div>
                <div class="bathroom">
                    <img src="img/icons/bathtub.png" alt="">
                    <span>{{ $property->bath }}</span>
                </div>
                <div class="garage">
                    <img src="img/icons/garage.png" alt="">
                    <span>{{ $property->garage }}</span>
                </div>
                <div class="space">
                    <img src="img/icons/space.png" alt="">
                    <span>{{ $property->area }}</span>
                </div>
            </div>
        </div>
    </div>
</div>
