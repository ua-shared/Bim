<div class="form-group">

    <label for="{{ $name }}">{{ $title }}
        <span class="sub_title">{{ isset($sub_title) ? $sub_title : '' }}</span>
    </label>

    <select id="{{ isset($id) ? $id : $name }}"

            class="form-control{{ $errors->has($name) ? ' is-invalid' : '' }}{{ isset($class) ? ' ' . $class : '' }}"
            name="{{ $name }}" {{ $required ? ' required' : ''}}
        {!! !empty($others) ? $others : '' !!}>

        @foreach($options as $val => $text)
            <option value="{{ $val }}"
                {{ old($name) == $val ? ' selected' : (!$errors->has($name) &&
                isset($selected) && $selected == $val ? 'selected' : '') }}>
                {{ empty($text) ? $val : $text }}</option>
        @endforeach

    </select>

    <span class="invalid-feedback feedback_span" role="alert">
            <strong>
                @if ($errors->has($name) && !isset($hideErrors))
                    {{ $errors->first($name) }}
                @endif
            </strong>
    </span>
</div>
