<div class="form-group row">
    <label for="{{ $name }}" class="col-md-5 col-form-label text-md-right">{{ $title }}</label>

    <div class="col-md-7">
        <input id="{{ $name }}" type="{{ $type }}" class="form-control{{ $errors->has($name) ? ' is-invalid' : '' }}"
               name="{{ $name }}"
               value="{{ old($name, isset($value) ? $value : '') }}" {{ $required ? 'required' : ''}}
            {!! !empty($others) ? $others : '' !!}>

        <span class="invalid-feedback feedback_span" role="alert">
            <strong>
                @if ($errors->has($name) && !isset($hideErrors))
                    {{ $errors->first($name) }}
                @endif
            </strong>
    </span>
    </div>
</div>
