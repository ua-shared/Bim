<div class="checkbox {{ isset($div_class) ? $div_class : '' }}">
    <label>
        <input type="checkbox" id="{{ isset($id) ? $id : $name }}" name="{{ $name }}"
               value="1" class="{{ isset($class) ? $class : '' }}
        {{ isset($big_size) ? 'form-control' : ''}} my_checkbox"
            {{ old($name, isset($checked) ? 'checked' : '') ? 'checked' : '' }}
            {!! !empty($others) ? $others : '' !!}>
        {!! $title !!}
    </label>

    <span class="invalid-feedback feedback_span" role="alert">
            <strong>
                @if ($errors->has($name) && !isset($hideErrors))
                    {{ $errors->first($name) }}
                @endif
            </strong>
    </span>
</div>
