<div class="form-group row">
    <label for="{{ $name }}" class="col-md-5 col-form-label text-md-right">{{ $title }}</label>

    <div class="col-md-7">
        <input id="{{ $name }}" type="{{ $type }}" class="form-control{{ $errors->has($name) ? ' is-invalid' : '' }}"
               name="{{ $name }}"
               value="{{ old($name, isset($value) ? $value : '') }}" {{ $required ? 'required' : ''}}
            {!! !empty($others) ? $others : '' !!}>

        @if ($errors->has($name))
            <span class="invalid-feedback" role="alert">
            <strong>{{ $errors->first($name) }}</strong>
        </span>
        @endif
    </div>
</div>
