<!DOCTYPE html>
<html lang="{{ str_replace('_', '-', app()->getLocale()) }}">
<head>
    <meta charset="UTF-8">
    <meta name="description" content="">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
    <!-- The above 4 meta tags *must* come first in the head; any other head content must come *after* these tags -->

    <meta name="author" content="Chanil">

    <title>{{ config('app.name', 'Laravel') }}</title>

    <!-- CSRF Token -->
    <meta name="csrf-token" content="{{ csrf_token() }}">

    <!-- Favicon  -->
    <link rel="icon" href="{{ asset("img/core-img/favicon.ico") }}">

    <!-- Style CSS -->
    <link rel="stylesheet" href="{{ asset("css/style.css") }}">
    <link rel="stylesheet" href="{{ asset("css/select2.css") }}">
    <link rel="stylesheet" href="{{ asset("css/app.css") }}">
</head>
<body id="body">

<div class="container">

    <textarea name="" id="" cols="30" rows="10"></textarea>


</div>


<!-- Scrips -->
<!-- jQuery (Necessary for All JavaScript Plugins) -->
<script src="{{ asset("js/jquery/jquery-2.2.4.min.js") }}"></script>
<!-- Popper js -->
<script src="{{ asset("js/popper.min.js") }}"></script>
<!-- Bootstrap js -->
<script src="{{ asset("js/bootstrap.js") }}"></script>
<!-- Plugins js -->
<script src="{{ asset("js/plugins.js") }}"></script>
<script src="{{ asset("js/classy-nav.min.js") }}"></script>
<script src="{{ asset("js/jquery-ui.min.js") }}"></script>
<!-- Active js -->
<script src="{{ asset("js/active.js") }}"></script>
<script src="{{ asset("js/select2/select2.js") }}"></script>
<script src="{{ asset("js/test.js") }}"></script>
</body>
</html>
