@extends('layouts.menu_app')

@section('page_specific_scripts')
    <script src="{{ asset("vendor/datatables/src/jquery.dataTables.js") }}"></script>
    <link rel="" href="{{ asset('data/data.json') }}">
    <script src="{{ asset("js/pages_js/properties_index_page.js") }}"></script>
@endsection

@section('page_specific_styles')
    <link rel="stylesheet" href="{{ asset('vendor/datatables/src/jquery.dataTables.css') }}"/>
    <style>
        td.details-control {
            background: url({{ asset('img/details_open.png') }}) no-repeat center center;
            cursor: pointer;
        }

        tr.shown td.details-control {
            background: url({{ asset('img/details_close.png') }}) no-repeat center center;
        }
    </style>
@endsection

@section('menu_app_content')
    <div class="row justify-content-center">
        <div class="col">
            <table id="propertiesList" class="display" style="width: 100%">
                <thead>
                <tr>
                    <th></th>
                    <th>Nom</th>
                    <th>Opération</th>
                    <th>Type</th>
                    <th>Ville</th>
                    <th>Quartier</th>
                    <th>Prix</th>
                    <th>Superficie <br> (en m²)</th>
                    <th>Date de création</th>
                </tr>
                </thead>

                <tfoot>
                <tr>
                    <th></th>
                    <th>Nom</th>
                    <th>Opération</th>
                    <th>Type</th>
                    <th>Ville</th>
                    <th>Quartier</th>
                    <th>Prix</th>
                    <th>Superficie <br> (en m²)</th>
                    <th>Date de création</th>
                </tr>
                </tfoot>
            </table>
        </div>
    </div>

    @foreach($ids as $id)
        <form id="deleteProperty{{ $id }}"
              action="{{ route('properties.destroy', $id) }}" method="POST">
            @csrf
            @method('DELETE')
        </form>
        <form id="forceDeleteProperty{{ $id }}"
              action="{{ route('properties.forceDestroy', $id) }}" method="POST">
            @csrf
            @method('DELETE')
        </form>
    @endforeach

@endsection
