@extends('layouts.app')

@section('page_specific_styles')
    <link rel="stylesheet" href="{{ asset("vendor/leaflet/leaflet.css") }}">
    <link rel="stylesheet" href="{{ asset("css/pages_css/edit_property_page.css") }}">
@endsection

@section('page_specific_scripts')
    <script src="{{ asset("vendor/leaflet/leaflet-src.js") }}"></script>
@endsection

@section('page_specific_scripts_2')
    <script src="{{ asset("js/pages_js/edit_property_page.js") }}"></script>
@endsection

@section('app_content')
    <div class="linear_gradient top_margin bottom_margin">
        <div class="container">
            <div class="row justify-content-center" id="create_property_page">
                <div class="col-md-10">
                    <div class="text-center mb-50"><h1>Mise à jour des informations de la proprieté</h1></div>

                @include('layouts.includes.flashMessages')

                <!--------------- ----------------->
                    <!--------------- IMAGES ---------->
                    <!--------------- ----------------->
                    <div class="card-columns">

                        <div class="card">
                            <a href="{{ url("properties/" . $property->ftdImage->name) }}" class="image-link">
                                <img class="card-img-top"
                                     src="{{ url("properties/thumbs/" . $property->ftdImage->name) }}" alt="image"></a>
                            <div class="card-footer">
                                <span class="pull-right text-right m-2" style="color: green;">
                                    <strong>Image vedette</strong>
                                </span>
                            </div>
                        </div>

                        @foreach($property->nonFtdImages as $image)
                            <div class="card">
                                <a href="{{ url("properties/" . $image->name) }}" class="image-link">
                                    <img class="card-img-top"
                                         src="{{ url("properties/thumbs/" . $image->name) }}" alt="image"></a>
                                <div class="card-footer text-muted">
                                    <small class="pull-right text-right m-2">
                                        <a class="" href="{{ route('image.set_featured', [
                                                                'property' => $property->id,
                                                                'image' => $image->id]) }}"
                                           data-toggle="tooltip"
                                           title="Choisir cette photo comme la photo vedette de cette proprieté">
                                            </i>Choisir comme vedette</a>
                                        <br>
                                        <a class="form-delete" href="{{ route('image.destroy', [
                                                                'property' => $property->id,
                                                                'image' => $image->id
                                                                ]) }}"
                                           data-toggle="tooltip" title='Supprimer cette photo'><i
                                                class="fa fa-trash"></i>&nbsp;Supprimer</a>

                                        <form action="{{ route('image.destroy', [
                                                                'property' => $property->id,
                                                                'image' => $image->id]) }}"
                                              method="POST" class="hide">
                                            {{ csrf_field() }}
                                            {{ method_field('DELETE') }}
                                        </form>
                                    </small>
                                </div>
                            </div>
                        @endforeach
                    </div>
                    <div class="row mt-15">
                        <div class="col">
                            <a href="{{ route("properties.add_image", $property) }}" class="btn south-btn-small"
                               id="cancel_property_create">Ajouter une image</a>
                        </div>
                    </div>


                    <!--------------- ----------------->
                    <!--------------- FORMULAIRES ---------->
                    <!--------------- ----------------->
                    <form id="edit_property_form" enctype="multipart/form-data" method="POST"
                          action="{{ route('properties.update', $property) }}">
                    @csrf
                    @method('PUT')

                    <!-- fieldset 1 -->
                        <fieldset>

                            <div class="row mt-30 mb-15">
                                <div class="col-md-6">Voulez-vous vendre ou louer ?<br>
                                </div>
                                <div class="col-md-6 text-left" id="type_operation">
                                    <div id="type_operation">
                                        <div class="custom-control custom-radio mb-3">
                                            <input type="radio"
                                                   class="custom-control-input{{ $errors->has('type_operation') ? ' is-invalid' : '' }}"
                                                   id="type_operation_Louer"
                                                   value="Louer" name="type_operation"
                                                   required="" {{ $property->type_operation == 'Louer' ? ' checked' : '' }}>
                                            <label class="custom-control-label" for="type_operation_Louer">Louer</label>
                                        </div>
                                        <div class="custom-control custom-radio mb-3">
                                            <input type="radio"
                                                   class="custom-control-input{{ $errors->has('type_operation') ? ' is-invalid' : '' }}"
                                                   id="type_operation_Vendre"
                                                   value="Vendre" name="type_operation"
                                                   required="" {{ $property->type_operation == 'Vendre' ? ' checked' : '' }}>
                                            <label class="custom-control-label"
                                                   for="type_operation_Vendre">Vendre</label>
                                        </div>

                                    </div>
                                </div>
                            </div>

                            <div class="form-group">
                                <label for="name">Nom de la proprieté</label>
                                <input id="name" name="name" type="text"
                                       class="form-control{{ $errors->has('name') ? ' is-invalid' : '' }}"
                                       value="{{ $property->name }}" required>

                                @if ($errors->has('name'))
                                    <span class="invalid-feedback" role="alert">
                                        <strong>{{ $errors->first('name') }}</strong>
                                    </span>
                                @endif
                            </div>

                            <div class="row">
                                <div class="col">
                                    <div class="form-group">
                                        <label for="ville">Ville</label>
                                        <select id="ville"
                                                class="form-control{{ $errors->has('ville') ? ' is-invalid' : '' }}"
                                                name="ville" required="">
                                            @foreach($villes as $ville)
                                                <option
                                                    value="{{ $ville->name }}" {{ $property->ville == $ville->name ? 'selected' : '' }}>{{ $ville->name }}</option>
                                            @endforeach
                                        </select>

                                        @if ($errors->has('ville'))
                                            <span class="invalid-feedback" role="alert">
                                                <strong>{{ $errors->first('ville') }}</strong>
                                            </span>
                                        @endif
                                    </div>
                                </div>

                                <div class="col">
                                    <div class="form-group">

                                        <label for="quartier">Quartier
                                            <span class="sub_title"></span>
                                        </label>

                                        <input type="hidden" name="ancien_quatier" value="{{ $property->quartier }}">
                                        <select id="quartier"
                                                class="form-control{{ $errors->has('password') ? ' is-invalid' : '' }}"
                                                name="quartier" required="">
                                            <option value="{{ $property->quartier }}" selected="">
                                                {{ $property->quartier }}
                                            </option>
                                        </select>

                                        @if ($errors->has('quartier'))
                                            <span class="invalid-feedback" role="alert">
                                        <strong>{{ $errors->first('quartier') }}</strong>
                                    </span>
                                        @endif
                                    </div>
                                </div>
                            </div>

                            <div class="row">
                                <div class="col">
                                    <div class="form-group">
                                        <label for="prix">Prix (en FCFA)</label>
                                        <input id="prix" name="prix" type="number"
                                               class="form-control{{ $errors->has('prix') ? ' is-invalid' : '' }}"
                                               value="{{ $property->prix }}" required="">

                                        @if ($errors->has('prix'))
                                            <span class="invalid-feedback" role="alert">
                                        <strong>{{ $errors->first('prix') }}</strong>
                                    </span>
                                        @endif
                                    </div>
                                </div>

                                <div class="col">
                                    <div class="form-group">
                                        <label for="superficie">Superficie (en m²)</label>
                                        <input id="superficie" name="superficie" type="number"
                                               class="form-control{{ $errors->has('superficie') ? ' is-invalid' : '' }}"
                                               value="{{ $property->superficie }}" step="0.001">

                                        @if ($errors->has('superficie'))
                                            <span class="invalid-feedback" role="alert">
                                        <strong>{{ $errors->first('superficie') }}</strong>
                                    </span>
                                        @endif
                                    </div>
                                </div>
                            </div>

                            <div class="row">
                                <div class="col">
                                    <div class="form-group">

                                        <label for="type_propriete">Type de proprieté
                                            <span class="sub_title"></span>
                                        </label>

                                        <select id="type_propriete"
                                                class="form-control{{ $errors->has('password') ? ' is-invalid' : '' }}  nice_select"
                                                name="type_propriete" required="">

                                            <option value="">
                                                Choisissez une option
                                            </option>
                                            @foreach($types_propriete as $type_propriete)
                                                <option
                                                    {{ $property->type_propriete == $type_propriete->name ? 'selected' : '' }}
                                                    value="{{ $type_propriete->name }}">{{ $type_propriete->name }}</option>
                                            @endforeach
                                        </select>

                                        @if ($errors->has('type_propriete'))
                                            <span class="invalid-feedback" role="alert">
                                        <strong>{{ $errors->first('type_propriete') }}</strong>
                                    </span>
                                        @endif
                                    </div>
                                </div>
                                <div class="col">
                                    <div class="form-group">
                                        <label for="pieces">Nombre de pièces</label>
                                        <input id="pieces" name="pieces" type="number"
                                               class="form-control{{ $errors->has('pieces') ? ' is-invalid' : '' }}"
                                               value="{{ $property->pieces }}" required="">

                                        @if ($errors->has('pieces'))
                                            <span class="invalid-feedback" role="alert">

                                        @endif <strong>{{ $errors->first('pieces') }}</strong>
                                        </span>
                                    </div>
                                </div>
                            </div>

                            <div class="row">
                                <div class="col">
                                    <div class="form-group">
                                        <label for="lits">Nombre de lits</label>
                                        <input id="lits" name="lits" type="number"
                                               class="form-control{{ $errors->has('lits') ? ' is-invalid' : '' }}"
                                               value="{{ $property->lits }}"
                                               required="">

                                        @if ($errors->has('lits'))
                                            <span class="invalid-feedback" role="alert">
                                        <strong>{{ $errors->first('lits') }}</strong>
                                    </span>
                                        @endif
                                    </div>
                                </div>
                                <div class="col">
                                    <div class="form-group">
                                        <label for="douches">Nombre de douches</label>
                                        <input id="douches" name="douches" type="number"
                                               class="form-control{{ $errors->has('douches') ? ' is-invalid' : '' }}"
                                               value="{{ $property->douches }}"
                                               required="">

                                        @if ($errors->has('douches'))
                                            <span class="invalid-feedback" role="alert">
                                        <strong>{{ $errors->first('douches') }}</strong>
                                    </span>
                                        @endif
                                    </div>
                                </div>
                            </div>

                            <div class="row">
                                <div class="col">
                                    <div class="form-group">
                                        <label for="garages">Nombre de garages</label>
                                        <input id="garages" name="garages" type="number"
                                               class="form-control{{ $errors->has('garages') ? ' is-invalid' : '' }}"
                                               value="{{ $property->garages }}"
                                               required="">

                                        @if ($errors->has('garages'))
                                            <span class="invalid-feedback" role="alert">
                                        <strong>{{ $errors->first('garages') }}</strong>
                                    </span>
                                        @endif
                                    </div>
                                </div>
                                <div class="col">
                                    <div class="form-group">
                                        <label for="etages">Nombre d'étages</label>
                                        <input id="etages" name="etages" type="number"
                                               class="form-control{{ $errors->has('etages') ? ' is-invalid' : '' }}"
                                               value="{{ $property->etages }}"
                                               required="">

                                        @if ($errors->has('etages'))
                                            <span class="invalid-feedback" role="alert">
                                        <strong>{{ $errors->first('etages') }}</strong>
                                    </span>
                                        @endif
                                    </div>
                                </div>
                            </div>

                            <div class="form-group">
                                <label for="tags">Ajoutez des tags</label>
                                <select id="tags"
                                        class="form-control{{ $errors->has('tags') ? ' is-invalid' : '' }}"
                                        name="tags[]" multiple="">
                                    @foreach($property->tags as $tag)
                                        <option value="{{ $tag->name }}" selected>{{ $tag->name }}</option>
                                    @endforeach
                                </select>
                            </div>

                            <div class="form-group">
                                <label for="desc">Description (Autres détails)</label>
                                <textarea id="desc" name="desc" style="height: 100px;"
                                          class="form-control{{ $errors->has('desc') ? ' is-invalid' : '' }}">{{ $property->desc }}</textarea>

                                @if ($errors->has('desc'))
                                    <span class="invalid-feedback" role="alert">
                                        <strong>{{ $errors->first('desc') }}</strong>
                                    </span>
                                @endif
                            </div>

                            <div class="row">
                                <div class="col-md-4">
                                    <div class="checkbox text-center">
                                        <label>
                                            <input type="checkbox" id="piscine" name="piscine" value="1"
                                                   class="form-control{{ $errors->has('piscine') ? ' is-invalid' : '' }} my_checkbox"
                                                {{ $property->piscine ? 'checked' : '' }}>
                                            Piscine
                                        </label>

                                        @if ($errors->has('piscine'))
                                            <span class="invalid-feedback" role="alert">
                                                <strong>{{ $errors->first('piscine') }}</strong>
                                            </span>
                                        @endif
                                    </div>
                                </div>
                                <div class="col-md-4">
                                    <div class="checkbox text-center">
                                        <label>
                                            <input type="checkbox" id="jardin" name="jardin" value="1"
                                                   class="form-control{{ $errors->has('jardin') ? ' is-invalid' : '' }} my_checkbox"
                                                {{ $property->jardin ? 'checked' : '' }}>
                                            Jardin
                                        </label>

                                        @if ($errors->has('jardin'))
                                            <span class="invalid-feedback" role="alert">
                                                <strong>{{ $errors->first('jardin') }}</strong>
                                            </span>
                                        @endif
                                    </div>
                                </div>
                                <div class="col-md-4">
                                    <div class="checkbox text-center">
                                        <label>
                                            <input type="checkbox" id="vue_sur_mer" name="vue_sur_mer" value="1"
                                                   class="form-control{{ $errors->has('vue_sur_mer') ? ' is-invalid' : '' }} my_checkbox"
                                                {{ $property->vue_sur_mer ? 'checked' : '' }}>
                                            Vue sur mer
                                        </label>

                                        @if ($errors->has('vue_sur_mer'))
                                            <span class="invalid-feedback" role="alert">
                                        <strong>{{ $errors->first('vue_sur_mer') }}</strong>
                                    </span>
                                        @endif
                                    </div>
                                </div>
                            </div>

                        </fieldset>

                        <!-- fieldset 2 -->
                        <fieldset>

                            <legend>Géolocalisation</legend>

                            <p>
                                Indiquez la position approximative de la proprieté sur la carte.
                            </p>

                            <div class="row">
                                <div class="col">
                                    <div id="my_map"></div>
                                </div>
                            </div>

                        </fieldset>

                        <div class="checkbox">
                            <label><input type="checkbox" name="certif" id="certif" value="yes" checked="">
                                &nbsp;&nbsp;Je certifie que les informations sus-mentionnées sont exactes
                            </label>
                        </div>

                        <div class="row">
                            <div class="col">
                                <p>
                                    <input type="hidden" name="lat" id="lat" value="{{ $property->lat }}">
                                    <input type="hidden" name="lng" id="lng" value="{{ $property->lng }}">
                                    <span class="invalid-feedback feedback_span" role="alert" id="global_error"></span>
                                </p>
                            </div>
                        </div>

                        <div class="row text-center mt-15">
                            <div class="col">
                                <a href="{{ route("home") }}" class="btn south-btn"
                                   id="cancel_property_create">Annuler</a>
                            </div>
                            <div class="col">
                                <input type="submit" name="update_property" id="update_property" class="btn south-btn"
                                       value="Mettre à jour">
                            </div>
                        </div>
                    </form>

                </div>
            </div>
        </div>
    </div>
@endsection
