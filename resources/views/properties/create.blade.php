@extends('layouts.app')

@section('page_specific_styles')
    <link rel="stylesheet" href="{{ asset("vendor/leaflet/leaflet.css") }}">
    <link rel="stylesheet" href="{{ asset("css/pages_css/create_property_page.css") }}">
@endsection

@section('page_specific_scripts')
    <script src="{{ asset("vendor/leaflet/leaflet-src.js") }}"></script>
@endsection

@section('page_specific_scripts_2')
    <script src="{{ asset("js/pages_js/create_property_page.js") }}"></script>
@endsection

@section('app_content')
    <div class="linear_gradient top_margin bottom_margin">
        <div class="container">
            <div class="row justify-content-center" id="create_property_page">
                <div class="col-md-10">
                    <div class="text-center"><h1>Nouvelle proprieté</h1></div>

                    @include('layouts.includes.flashMessages')

                    {{--@isset($errors)
                        {{ var_dump($errors) }}
                    @endif--}}

                    <form id="create_property_form" enctype="multipart/form-data" method="POST"
                          action="{{ route('properties.store') }}">
                    @csrf

                    <!-- fieldset 1 -->
                        <fieldset>

                            <div class="row mt-30 mb-15">
                                <div class="col-md-6">Voulez-vous vendre ou louer ?<br>
                                </div>
                                <div class="col-md-6 text-left" id="type_operation">
                                    @include('partials.radio_btns', [
                                    'name' => 'type_operation',
                                    'group_id' => 'type_operation',
                                    'options' => [
                                        'Louer' => 'Louer',
                                        'Vendre' => 'Vendre',
                                    ],
                                    ])
                                </div>
                            </div>

                            @include('partials.input_simple_lines', [
                                'title' => "Nom de la proprieté",
                                'type' => 'text',
                                'name' => 'name',
                                'required' => true,
                                'value' => 'Prop01',
                                ])

                            <div class="row">
                                <div class="col">
                                    <div class="form-group">
                                        <label for="ville">Ville</label>
                                        <select id="ville"
                                                class="form-control{{ $errors->has('ville') ? ' is-invalid' : '' }}"
                                                name="ville" required="">
                                            @foreach($villes as $ville)
                                                <option
                                                    value="{{ $ville->name }}">{{ $ville->name }}</option>
                                            @endforeach
                                        </select>

                                        @if ($errors->has('ville'))
                                            <span class="invalid-feedback" role="alert">
                                                <strong>{{ $errors->first('ville') }}</strong>
                                            </span>
                                        @endif
                                    </div>
                                </div>
                                <div class="col">
                                    @include('partials.select', [
                                       'title' => "Quartier",
                                       'name' => 'quartier',
                                       'required' => true,
                                       'selected' => 'zogbo',
                                       'options' => [
                                       '' => 'Choisissez un quartier',
                                       'zogbo' => 'Zogbo',
                                       ' ' => "Choisissez d'abord une ville",
                                       '  ' => 'Chargement en cours . . .',
                                       ],
                                       ])
                                </div>
                            </div>

                            <div class="row">
                                <div class="col">
                                    @include('partials.input_simple_lines', [
                                'title' => "Prix (en FCFA)",
                                'type' => 'number',
                                'name' => 'prix',
                                'required' => true,
                                'value' => '120000',
                                ])
                                </div>
                                <div class="col">
                                    @include('partials.input_simple_lines', [
                                        'title' => "Superficie (en m²)",
                                        'type' => 'number',
                                        'name' => 'superficie',
                                        'required' => false,
                                        'value' => '45',
                                        'others' => 'step="0.001"'
                                        ])
                                </div>
                            </div>

                            <div class="row">
                                <div class="col">
                                    @include('partials.select', [
                                       'title' => "Type de proprieté",
                                       'name' => 'type_propriete',
                                       'required' => true,
                                       'selected' => 'Villa',
                                       'class' => ' nice_select',
                                       'options' => [
                                       '' => 'Choisissez une option',
                                       'Appartements' => '',
                                        'Bureaux' => '',
                                        'Boutiques' => '',
                                        'Immeubles' => '',
                                        'Locations meublées' => '',
                                        'Magasins' => '',
                                        'Maisons-Villas' => '',
                                        'Terrains-Parcelles' => '',
                                        'Residences etudiantes' => '',
                                       ],
                                       ])
                                </div>
                                <div class="col">
                                    @include('partials.input_simple_lines', [
                                        'title' => "Nombre de pièces",
                                        'type' => 'number',
                                        'name' => 'pieces',
                                        'required' => true,
                                        'value' => '1',
                                        ])
                                </div>
                            </div>

                            <div class="row">
                                <div class="col">
                                    @include('partials.input_simple_lines', [
                                       'title' => "Nombre de lits",
                                        'type' => 'number',
                                       'name' => 'lits',
                                       'required' => false,
                                        'required' => true,
                                        'value' => '2',
                                       ])
                                </div>
                                <div class="col">
                                    @include('partials.input_simple_lines', [
                                        'title' => "Nombre de douches",
                                        'type' => 'number',
                                        'name' => 'douches',
                                        'required' => true,
                                        'value' => '',
                                        'required' => true,
                                        'value' => '3',
                                        ])
                                </div>
                            </div>

                            <div class="row">
                                <div class="col">
                                    @include('partials.input_simple_lines', [
                                       'title' => "Nombre de garages",
                                        'type' => 'number',
                                       'name' => 'garages',
                                       'required' => true,
                                        'required' => true,
                                        'value' => '4',
                                       ])
                                </div>
                                <div class="col">
                                    @include('partials.input_simple_lines', [
                                        'title' => "Nombre d'étages",
                                        'type' => 'number',
                                        'name' => 'etages',
                                        'required' => true,
                                        'value' => '',
                                        'required' => true,
                                        'value' => '5',
                                        ])
                                </div>
                            </div>

                            <div class="form-group">
                                <label for="tags">Ajoutez des tags</label>
                                <select id="tags" class="form-control" name="tags[]" multiple="">
                                    <option value="peace" selected>peace</option>
                                    <option value="luxe" selected>luxe</option>
                                    <option value="spacieux" selected>spacieux</option>
                                </select>
                            </div>

                            @include('partials.textarea', [
                        'title' => 'Description (Autres détails)',
                        'name' => 'desc',
                        'value' => 'Desc',
                        ])

                            <div class="row">
                                <div class="col-md-4">
                                    @include('partials.checkbox', [
                                       'title' => "Piscine",
                                       'name' => 'piscine',
                                       'big_size' => true,
                                       'div_class' => 'text-center',
                                       'checked' => true,
                                       ])
                                </div>
                                <div class="col-md-4">
                                    @include('partials.checkbox', [
                                       'title' => "Jardin",
                                       'name' => 'jardin',
                                       'big_size' => true,
                                       'div_class' => 'text-center',
                                       ])
                                </div>
                                <div class="col-md-4">
                                    @include('partials.checkbox', [
                                       'title' => "Vue sur mer",
                                       'name' => 'vue_sur_mer',
                                       'big_size' => true,
                                       'div_class' => 'text-center',
                                       'checked' => true,
                                       ])
                                </div>
                            </div>

                        </fieldset>

                        <!-- fieldset 2 -->
                        <fieldset>
                            <legend class="mb-3">Photos (Uniquement aux formats .jpg et .png)</legend>

                            <div id='property_images'>

                                <!-- Image Vedette -->
                                <div id="img_vdt">
                                    <div class="row mb-3">
                                        <div class="col">
                                            <h5>Choisissez l'image vedette</h5>
                                        </div>
                                    </div>
                                    <div class="row mb-3">
                                        <div class="col-md-6">
                                            <div class="img_input_div">
                                                <div class='img_preview'>
                                                    <label>Cliquez ici pour selectionner une image</label>
                                                </div>
                                                <input type="file" name="img_vdt_file" class='img_input'
                                                       required/>
                                            </div>
                                            <span class="invalid-feedback showMyFeeadback" role="alert">
                                                <strong>
                                                    @if ($errors->has('img_vdt_file'))
                                                        {{ $errors->first('img_vdt_file') }}
                                                    @endif
                                                </strong>
                                            </span>
                                        </div>
                                    </div>
                                </div>

                                <!-- Autres Images -->
                                <div id="img_others">
                                    <div class="row mb-3">
                                        <div class="col-md-6">
                                            <h5>Choisissez les autres images (Au maximum 5)</h5>
                                        </div>
                                    </div>
                                    <div class="row" id='property_img_others'>
                                        <div class="col">
                                            <div class="row">
                                                <div class="col-md-6 img_input_div">
                                                    <div class='img_preview'>
                                                        <label>Cliquez ici pour selectionner une image</label>
                                                    </div>
                                                    <input type="file" name="img_others_files[]" class='img_input'
                                                           required/>
                                                </div>
                                                @for($i = 4; $i > 0; $i--)
                                                    <div class="col-md-6 img_input_div hidden_img_input">
                                                        <div class='img_preview'>
                                                            <label>Cliquez ici pour selectionner une image</label>
                                                        </div>
                                                        <input type="file" name="img_others_files[]" class='img_input'/>
                                                        <div class="mb-3 ml-2">
                                                            <button class="btn delete_img_input_btn">
                                                                Supprimer
                                                            </button>
                                                        </div>
                                                    </div>
                                                @endfor
                                            </div>
                                        </div>
                                        @if ($errors->has('img_others_files'))
                                            <div class="row">
                                                <div class="col">
                                                    <span class="invalid-feedback showMyFeeadback">
                                                        <strong>{{ $errors->first('img_others_files') }}</strong>
                                                    </span>
                                                </div>
                                            </div>
                                        @endif
                                    </div>

                                    <div class="row">
                                        <div class="col">
                                            <button id="add_img_btn" class="btn south-btn">
                                                Ajouter une image
                                            </button>
                                            <p class='invalid-feedback feedback_span' role='alert'>
                                                Vous ne pouvez plus ajouter d'autres images</p>
                                        </div>
                                    </div>
                                </div>

                            </div>

                        </fieldset>

                        <!-- fieldset 3 -->
                        <fieldset>

                            <legend>Géolocalisation</legend>

                            <p>
                                Indiquez la position approximative de la proprieté sur la carte.
                            </p>

                            <div class="row">
                                <div class="col">
                                    <div id="my_map"></div>
                                </div>
                            </div>

                            @if ($errors->has('lat') || $errors->has('lng'))
                                <div class="row">
                                    <div class="col">
                                        <span class="invalid-feedback showMyFeeadback">
                                            <strong>Indiquez la position approximative de votre proprieté sur la carte.</strong>
                                        </span>
                                    </div>
                                </div>
                            @endif

                        </fieldset>

                        <div class="checkbox">
                            <label><input type="checkbox" name="certif" id="certif"
                                          value="yes" {{ old('accept_locate', 'checked') ? 'checked' : '' }}>
                                &nbsp;&nbsp;Je certifie que les informations sus-mentionnées sont exactes
                            </label>
                        </div>

                        <div class="row">
                            <div class="col">
                                <p>
                                    <input type="hidden" name="lat" id="lat" value="">
                                    <input type="hidden" name="lng" id="lng" value="">
                                    <span class="invalid-feedback feedback_span" role="alert" id="global_error"></span>
                                </p>
                            </div>
                        </div>

                        <div class="row text-center mt-15">
                            <div class="col">
                                <a href="{{ route("home") }}" class="btn south-btn"
                                   id="cancel_property_create">Annuler</a>
                            </div>
                            <div class="col">
                                <input type="submit" name="create_property" id="create_property"
                                       class="btn south-btn" value="Créer"/>
                            </div>
                        </div>
                    </form>

                </div>
            </div>
        </div>
    </div>
@endsection
