@extends('layouts.app')

@section('page_specific_styles')
    <link rel="stylesheet" href="{{ asset("vendor/leaflet/leaflet.css") }}">
@endsection

@section('page_specific_scripts')
    <script src="{{ asset("vendor/leaflet/leaflet-src.js") }}"></script>
@endsection

@section('page_specific_scripts_2')
    <script src="{{ asset("js/pages_js/show_property_page.js") }}"></script>
@endsection

@section('app_content')
                <!-- ##### Breadcumb Area Start ##### -->
                <section class="breadcumb-area bg-img" style="background-image: url({{ asset('img/bg-img/hero1.jpg') }});">
                    <div class="container h-100">
                        <div class="row h-100 align-items-center">
                            <div class="col-12">
                                <div class="breadcumb-content">
                                    <h3 class="breadcumb-title">Property</h3>
                                </div>
                            </div>
                        </div>
                    </div>
                </section>
                <!-- ##### Breadcumb Area End ##### -->

                <!-- ##### Advance Search Area Start ##### -->
                <div class="south-search-area">
                    <div class="container">
                        <div class="row">
                            <div class="col-12">
                                <div class="advanced-search-form">
                                    <!-- Search Title -->
                                    <div class="search-title">
                                        <p>Search for your home</p>
                                    </div>
                                    <!-- Search Form -->
                                    <form action="#" method="post" id="advanceSearch">
                                        <div class="row">

                                            <div class="col-12 col-md-4 col-lg-3">
                                                <div class="form-group">
                                                    <input type="input" class="form-control" name="input"
                                                           placeholder="Keyword">
                                                </div>
                                            </div>

                                            <div class="col-12 col-md-4 col-lg-3">
                                                <div class="form-group">
                                                    <select class="form-control" id="cities" style="display: none;">
                                                        <option>All Cities</option>
                                                        <option>Riga</option>
                                                        <option>Melbourne</option>
                                                        <option>Vienna</option>
                                                        <option>Vancouver</option>
                                                        <option>Toronto</option>
                                                        <option>Calgary</option>
                                                        <option>Adelaide</option>
                                                        <option>Perth</option>
                                                        <option>Auckland</option>
                                                        <option>Helsinki</option>
                                                    </select>
                                                    <div class="nice-select form-control" tabindex="0"><span
                                                            class="current">All Cities</span>
                                                        <ul class="list">
                                                            <li data-value="All Cities" class="option selected">All
                                                                Cities
                                                            </li>
                                                            <li data-value="Riga" class="option">Riga</li>
                                                            <li data-value="Melbourne" class="option">Melbourne</li>
                                                            <li data-value="Vienna" class="option">Vienna</li>
                                                            <li data-value="Vancouver" class="option">Vancouver</li>
                                                            <li data-value="Toronto" class="option">Toronto</li>
                                                            <li data-value="Calgary" class="option">Calgary</li>
                                                            <li data-value="Adelaide" class="option">Adelaide</li>
                                                            <li data-value="Perth" class="option">Perth</li>
                                                            <li data-value="Auckland" class="option">Auckland</li>
                                                            <li data-value="Helsinki" class="option">Helsinki</li>
                                                        </ul>
                                                    </div>
                                                </div>
                                            </div>

                                            <div class="col-12 col-md-4 col-lg-3">
                                                <div class="form-group">
                                                    <select class="form-control" id="catagories" style="display: none;">
                                                        <option>All Catagories</option>
                                                        <option>Apartment</option>
                                                        <option>Bar</option>
                                                        <option>Farm</option>
                                                        <option>House</option>
                                                        <option>Store</option>
                                                    </select>
                                                    <div class="nice-select form-control" tabindex="0"><span
                                                            class="current">All Catagories</span>
                                                        <ul class="list">
                                                            <li data-value="All Catagories" class="option selected">All
                                                                Catagories
                                                            </li>
                                                            <li data-value="Apartment" class="option">Apartment</li>
                                                            <li data-value="Bar" class="option">Bar</li>
                                                            <li data-value="Farm" class="option">Farm</li>
                                                            <li data-value="House" class="option">House</li>
                                                            <li data-value="Store" class="option">Store</li>
                                                        </ul>
                                                    </div>
                                                </div>
                                            </div>

                                            <div class="col-12 col-md-4 col-lg-3">
                                                <div class="form-group">
                                                    <select class="form-control" id="offers" style="display: none;">
                                                        <option>All Offers</option>
                                                        <option>100% OFF</option>
                                                        <option>75% OFF</option>
                                                        <option>50% OFF</option>
                                                        <option>25% OFF</option>
                                                        <option>10% OFF</option>
                                                    </select>
                                                    <div class="nice-select form-control" tabindex="0"><span
                                                            class="current">All Offers</span>
                                                        <ul class="list">
                                                            <li data-value="All Offers" class="option selected">All
                                                                Offers
                                                            </li>
                                                            <li data-value="100% OFF" class="option">100% OFF</li>
                                                            <li data-value="75% OFF" class="option">75% OFF</li>
                                                            <li data-value="50% OFF" class="option">50% OFF</li>
                                                            <li data-value="25% OFF" class="option">25% OFF</li>
                                                            <li data-value="10% OFF" class="option">10% OFF</li>
                                                        </ul>
                                                    </div>
                                                </div>
                                            </div>

                                            <div class="col-12 col-md-4 col-xl-3">
                                                <div class="form-group">
                                                    <select class="form-control" id="listings" style="display: none;">
                                                        <option>All Listings</option>
                                                        <option>Listings 1</option>
                                                        <option>Listings 2</option>
                                                        <option>Listings 3</option>
                                                        <option>Listings 4</option>
                                                        <option>Listings 5</option>
                                                    </select>
                                                    <div class="nice-select form-control" tabindex="0"><span
                                                            class="current">All Listings</span>
                                                        <ul class="list">
                                                            <li data-value="All Listings" class="option selected">All
                                                                Listings
                                                            </li>
                                                            <li data-value="Listings 1" class="option">Listings 1</li>
                                                            <li data-value="Listings 2" class="option">Listings 2</li>
                                                            <li data-value="Listings 3" class="option">Listings 3</li>
                                                            <li data-value="Listings 4" class="option">Listings 4</li>
                                                            <li data-value="Listings 5" class="option">Listings 5</li>
                                                        </ul>
                                                    </div>
                                                </div>
                                            </div>

                                            <div class="col-12 col-md-4 col-xl-2">
                                                <div class="form-group">
                                                    <select class="form-control" id="bedrooms" style="display: none;">
                                                        <option>Bedrooms</option>
                                                        <option>1</option>
                                                        <option>2</option>
                                                        <option>3</option>
                                                        <option>4</option>
                                                        <option>5+</option>
                                                    </select>
                                                    <div class="nice-select form-control" tabindex="0"><span
                                                            class="current">Bedrooms</span>
                                                        <ul class="list">
                                                            <li data-value="Bedrooms" class="option selected">Bedrooms
                                                            </li>
                                                            <li data-value="1" class="option">1</li>
                                                            <li data-value="2" class="option">2</li>
                                                            <li data-value="3" class="option">3</li>
                                                            <li data-value="4" class="option">4</li>
                                                            <li data-value="5+" class="option">5+</li>
                                                        </ul>
                                                    </div>
                                                </div>
                                            </div>

                                            <div class="col-12 col-md-4 col-xl-2">
                                                <div class="form-group">
                                                    <select class="form-control" id="bathrooms" style="display: none;">
                                                        <option>Bathrooms</option>
                                                        <option>1</option>
                                                        <option>2</option>
                                                        <option>3</option>
                                                        <option>4</option>
                                                        <option>5+</option>
                                                    </select>
                                                    <div class="nice-select form-control" tabindex="0"><span
                                                            class="current">Bathrooms</span>
                                                        <ul class="list">
                                                            <li data-value="Bathrooms" class="option selected">
                                                                Bathrooms
                                                            </li>
                                                            <li data-value="1" class="option">1</li>
                                                            <li data-value="2" class="option">2</li>
                                                            <li data-value="3" class="option">3</li>
                                                            <li data-value="4" class="option">4</li>
                                                            <li data-value="5+" class="option">5+</li>
                                                        </ul>
                                                    </div>
                                                </div>
                                            </div>

                                            <div class="col-12 col-md-8 col-lg-12 col-xl-5 d-flex">
                                                <!-- Space Range -->
                                                <div class="slider-range">
                                                    <div data-min="120" data-max="820" data-unit=" sq. ft"
                                                         class="slider-range-price ui-slider ui-slider-horizontal ui-widget ui-widget-content ui-corner-all"
                                                         data-value-min="120" data-value-max="820">
                                                        <div
                                                            class="ui-slider-range ui-widget-header ui-corner-all"></div>
                                                        <span class="ui-slider-handle ui-state-default ui-corner-all"
                                                              tabindex="0" style="left: 0%;"></span>
                                                        <span class="ui-slider-handle ui-state-default ui-corner-all"
                                                              tabindex="0" style="left: 100%;"></span>
                                                        <div class="ui-slider-range ui-corner-all ui-widget-header"
                                                             style="left: 0%; width: 100%;"></div>
                                                    </div>
                                                    <div class="range">120 sq. ft - 820 sq. ft</div>
                                                </div>

                                                <!-- Distance Range -->
                                                <div class="slider-range">
                                                    <div data-min="10" data-max="1300" data-unit=" mil"
                                                         class="slider-range-price ui-slider ui-slider-horizontal ui-widget ui-widget-content ui-corner-all"
                                                         data-value-min="10" data-value-max="1300">
                                                        <div
                                                            class="ui-slider-range ui-widget-header ui-corner-all"></div>
                                                        <span class="ui-slider-handle ui-state-default ui-corner-all"
                                                              tabindex="0" style="left: 0%;"></span>
                                                        <span class="ui-slider-handle ui-state-default ui-corner-all"
                                                              tabindex="0" style="left: 100%;"></span>
                                                        <div class="ui-slider-range ui-corner-all ui-widget-header"
                                                             style="left: 0%; width: 100%;"></div>
                                                    </div>
                                                    <div class="range">10 mil - 1300 mil</div>
                                                </div>
                                            </div>

                                            <div class="col-12 search-form-second-steps">
                                                <div class="row">

                                                    <div class="col-12 col-md-4 col-lg-3">
                                                        <div class="form-group">
                                                            <select class="form-control" id="types"
                                                                    style="display: none;">
                                                                <option>All Types</option>
                                                                <option>Apartment (30)</option>
                                                                <option>Land (69)</option>
                                                                <option>Villas (103)</option>
                                                            </select>
                                                            <div class="nice-select form-control" tabindex="0"><span
                                                                    class="current">All Types</span>
                                                                <ul class="list">
                                                                    <li data-value="All Types" class="option selected">
                                                                        All Types
                                                                    </li>
                                                                    <li data-value="Apartment (30)" class="option">
                                                                        Apartment (30)
                                                                    </li>
                                                                    <li data-value="Land (69)" class="option">Land
                                                                        (69)
                                                                    </li>
                                                                    <li data-value="Villas (103)" class="option">Villas
                                                                        (103)
                                                                    </li>
                                                                </ul>
                                                            </div>
                                                        </div>
                                                    </div>

                                                    <div class="col-12 col-md-4 col-lg-3">
                                                        <div class="form-group">
                                                            <select class="form-control" id="catagories2"
                                                                    style="display: none;">
                                                                <option>All Catagories</option>
                                                                <option>Apartment</option>
                                                                <option>Bar</option>
                                                                <option>Farm</option>
                                                                <option>House</option>
                                                                <option>Store</option>
                                                            </select>
                                                            <div class="nice-select form-control" tabindex="0"><span
                                                                    class="current">All Catagories</span>
                                                                <ul class="list">
                                                                    <li data-value="All Catagories"
                                                                        class="option selected">All Catagories
                                                                    </li>
                                                                    <li data-value="Apartment" class="option">
                                                                        Apartment
                                                                    </li>
                                                                    <li data-value="Bar" class="option">Bar</li>
                                                                    <li data-value="Farm" class="option">Farm</li>
                                                                    <li data-value="House" class="option">House</li>
                                                                    <li data-value="Store" class="option">Store</li>
                                                                </ul>
                                                            </div>
                                                        </div>
                                                    </div>

                                                    <div class="col-12 col-md-4 col-lg-3">
                                                        <div class="form-group">
                                                            <select class="form-control" id="Actions"
                                                                    style="display: none;">
                                                                <option>All Actions</option>
                                                                <option>Sales</option>
                                                                <option>Booking</option>
                                                            </select>
                                                            <div class="nice-select form-control" tabindex="0"><span
                                                                    class="current">All Actions</span>
                                                                <ul class="list">
                                                                    <li data-value="All Actions"
                                                                        class="option selected">All Actions
                                                                    </li>
                                                                    <li data-value="Sales" class="option">Sales</li>
                                                                    <li data-value="Booking" class="option">Booking</li>
                                                                </ul>
                                                            </div>
                                                        </div>
                                                    </div>

                                                    <div class="col-12 col-md-4 col-lg-3">
                                                        <div class="form-group">
                                                            <select class="form-control" id="city2"
                                                                    style="display: none;">
                                                                <option>All City</option>
                                                                <option>City 1</option>
                                                                <option>City 2</option>
                                                                <option>City 3</option>
                                                            </select>
                                                            <div class="nice-select form-control" tabindex="0"><span
                                                                    class="current">All City</span>
                                                                <ul class="list">
                                                                    <li data-value="All City" class="option selected">
                                                                        All City
                                                                    </li>
                                                                    <li data-value="City 1" class="option">City 1</li>
                                                                    <li data-value="City 2" class="option">City 2</li>
                                                                    <li data-value="City 3" class="option">City 3</li>
                                                                </ul>
                                                            </div>
                                                        </div>
                                                    </div>

                                                    <div class="col-12 col-md-4">
                                                        <div class="form-group">
                                                            <select class="form-control" id="Actions3"
                                                                    style="display: none;">
                                                                <option>All Actions</option>
                                                                <option>Sales</option>
                                                                <option>Booking</option>
                                                            </select>
                                                            <div class="nice-select form-control" tabindex="0"><span
                                                                    class="current">All Actions</span>
                                                                <ul class="list">
                                                                    <li data-value="All Actions"
                                                                        class="option selected">All Actions
                                                                    </li>
                                                                    <li data-value="Sales" class="option">Sales</li>
                                                                    <li data-value="Booking" class="option">Booking</li>
                                                                </ul>
                                                            </div>
                                                        </div>
                                                    </div>

                                                    <div class="col-12 col-md-4">
                                                        <div class="form-group">
                                                            <select class="form-control" id="city3"
                                                                    style="display: none;">
                                                                <option>All City</option>
                                                                <option>City 1</option>
                                                                <option>City 2</option>
                                                                <option>City 3</option>
                                                            </select>
                                                            <div class="nice-select form-control" tabindex="0"><span
                                                                    class="current">All City</span>
                                                                <ul class="list">
                                                                    <li data-value="All City" class="option selected">
                                                                        All City
                                                                    </li>
                                                                    <li data-value="City 1" class="option">City 1</li>
                                                                    <li data-value="City 2" class="option">City 2</li>
                                                                    <li data-value="City 3" class="option">City 3</li>
                                                                </ul>
                                                            </div>
                                                        </div>
                                                    </div>

                                                    <div class="col-12 col-md-4">
                                                        <div class="form-group">
                                                            <select class="form-control" id="city5"
                                                                    style="display: none;">
                                                                <option>All City</option>
                                                                <option>City 1</option>
                                                                <option>City 2</option>
                                                                <option>City 3</option>
                                                            </select>
                                                            <div class="nice-select form-control" tabindex="0"><span
                                                                    class="current">All City</span>
                                                                <ul class="list">
                                                                    <li data-value="All City" class="option selected">
                                                                        All City
                                                                    </li>
                                                                    <li data-value="City 1" class="option">City 1</li>
                                                                    <li data-value="City 2" class="option">City 2</li>
                                                                    <li data-value="City 3" class="option">City 3</li>
                                                                </ul>
                                                            </div>
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>

                                            <div class="col-12 d-flex justify-content-between align-items-end">
                                                <!-- More Filter -->
                                                <div class="more-filter">
                                                    <a href="#" id="moreFilter">+ More filters</a>
                                                </div>
                                                <!-- Submit -->
                                                <div class="form-group mb-0">
                                                    <button type="submit" class="btn south-btn">Search</button>
                                                </div>
                                            </div>
                                        </div>
                                    </form>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
                <!-- ##### Advance Search Area End ##### -->

                <!-- ##### Listings Content Area Start ##### -->
                <section class="listings-content-wrapper section-padding-100">
                    <div class="container">
                        <div class="row">
                            <div class="col-12">
                                <!-- Single Listings Slides -->
                                <div class="single-listings-sliders owl-carousel owl-theme owl-loaded">
                                    <!-- Single Slide -->

                                    <!-- Single Slide -->

                                    <div class="owl-stage-outer">
                                        <div class="owl-stage"
                                             style="transform: translate3d(-2070px, 0px, 0px); transition: 1s; width: 4140px;">
                                            <div class="owl-item cloned" style="width: 690px; margin-right: 0px;"><img
                                                    src="{{ asset('img/bg-img/hero4.jpg') }}" alt=""></div>
                                            <div class="owl-item cloned" style="width: 690px; margin-right: 0px;"><img
                                                    src="{{ asset('img/bg-img/hero5.jpg') }}" alt=""></div>
                                            <div class="owl-item" style="width: 690px; margin-right: 0px;"><img
                                                    src="{{ asset('img/bg-img/hero4.jpg') }}" alt=""></div>
                                            <div class="owl-item active" style="width: 690px; margin-right: 0px;"><img
                                                    src="{{ asset('img/bg-img/hero5.jpg') }}" alt=""></div>
                                            <div class="owl-item cloned" style="width: 690px; margin-right: 0px;"><img
                                                    src="{{ asset('img/bg-img/hero4.jpg') }}" alt=""></div>
                                            <div class="owl-item cloned" style="width: 690px; margin-right: 0px;"><img
                                                    src="{{ asset('img/bg-img/hero5.jpg') }}" alt=""></div>
                                        </div>
                                    </div>
                                    <div class="owl-controls">
                                        <div class="owl-nav">
                                            <div class="owl-prev" style=""><i class="ti-angle-left"></i></div>
                                            <div class="owl-next" style=""><i class="ti-angle-right"></i></div>
                                        </div>
                                        <div class="owl-dots" style="">
                                            <div class="owl-dot"><span></span></div>
                                            <div class="owl-dot active"><span></span></div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>

                        <div class="row justify-content-center">
                            <div class="col-12 col-lg-8">
                                <div class="listings-content">
                                    <!-- Price -->
                                    <div class="list-price">
                                        <p>$945 679</p>
                                    </div>
                                    <h5>Town house with Modern Architecture</h5>
                                    <p class="location"><img src="{{ asset('img/icons/location.png') }}" alt="">Upper Road 3411, no.34
                                        CA</p>
                                    <p>Etiam nec odio vestibulum est mattis effic iturut magna. Pellentesque sit amet
                                        tellus blandit. Etiam nec odiomattis effic iturut magna. Pellentesque sit am et
                                        tellus blandit. Etiam nec odio vestibul. Etiam nec odio vestibulum est mat tis
                                        effic iturut magna. Curabitur rhoncus auctor eleifend. Fusce venenatis diam
                                        urna, eu pharetra arcu varius ac. Etiam cursus turpis lectus, id iaculis risus
                                        tempor id. Phasellus fringilla nisl sed sem scelerisque, eget aliquam magna
                                        vehicula.</p>
                                    <!-- Meta -->
                                    <div class="property-meta-data d-flex align-items-end">
                                        <div class="new-tag">
                                            <img src="{{ asset('img/icons/new.png') }}" alt="">
                                        </div>
                                        <div class="bathroom">
                                            <img src="{{ asset('img/icons/bathtub.png') }}" alt="">
                                            <span>2</span>
                                        </div>
                                        <div class="garage">
                                            <img src="{{ asset('img/icons/garage.png') }}" alt="">
                                            <span>2</span>
                                        </div>
                                        <div class="space">
                                            <img src="{{ asset('img/icons/space.png') }}" alt="">
                                            <span>120 sq ft</span>
                                        </div>
                                    </div>
                                    <!-- Core Features -->
                                    <ul class="listings-core-features d-flex align-items-center">
                                        <li><i class="fa fa-check" aria-hidden="true"></i> Gated Community</li>
                                        <li><i class="fa fa-check" aria-hidden="true"></i> Automatic Sprinklers</li>
                                        <li><i class="fa fa-check" aria-hidden="true"></i> Fireplace</li>
                                        <li><i class="fa fa-check" aria-hidden="true"></i> Window Shutters</li>
                                        <li><i class="fa fa-check" aria-hidden="true"></i> Ocean Views</li>
                                        <li><i class="fa fa-check" aria-hidden="true"></i> Heated Floors</li>
                                        <li><i class="fa fa-check" aria-hidden="true"></i> Heated Floors</li>
                                        <li><i class="fa fa-check" aria-hidden="true"></i> Private Patio</li>
                                        <li><i class="fa fa-check" aria-hidden="true"></i> Window Shutters</li>
                                        <li><i class="fa fa-check" aria-hidden="true"></i> Fireplace</li>
                                        <li><i class="fa fa-check" aria-hidden="true"></i> Beach Access</li>
                                        <li><i class="fa fa-check" aria-hidden="true"></i> Rooftop Terrace</li>
                                    </ul>
                                    <!-- Listings Btn Groups -->
                                    <div class="listings-btn-groups">
                                        <a href="#" class="btn south-btn">See Floor plans</a>
                                        <a href="#" class="btn south-btn active">calculate mortgage</a>
                                    </div>
                                </div>
                            </div>
                            <div class="col-12 col-md-6 col-lg-4">
                                <div class="contact-realtor-wrapper">
                                    <div class="realtor-info">
                                        <img src="{{ asset('img/bg-img/listing.jpg') }}" alt="">
                                        <div class="realtor---info">
                                            <h2>Jeremy Scott</h2>
                                            <p>Realtor</p>
                                            <h6><img src="{{ asset('img/icons/phone-call.png') }}" alt=""> +45 677 8993000 223</h6>
                                            <h6><img src="{{ asset('img/icons/envelope.png') }}" alt=""> office@template.com</h6>
                                        </div>
                                        <div class="realtor--contact-form">
                                            <form action="#" method="post">
                                                <div class="form-group">
                                                    <input type="text" class="form-control" id="realtor-name"
                                                           placeholder="Your Name">
                                                </div>
                                                <div class="form-group">
                                                    <input type="number" class="form-control" id="realtor-number"
                                                           placeholder="Your Number">
                                                </div>
                                                <div class="form-group">
                                                    <input type="enumber" class="form-control" id="realtor-email"
                                                           placeholder="Your Mail">
                                                </div>
                                                <div class="form-group">
                                                    <textarea name="message" class="form-control" id="realtor-message"
                                                              cols="30" rows="10" placeholder="Your Message"></textarea>
                                                </div>
                                                <button type="submit" class="btn south-btn">Send Message</button>
                                            </form>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <!-- Listing Maps -->
                        <div class="row">
                            <div class="col-12">
                                <div class="listings-maps mt-100">
                                    <div id="googleMap"></div>
                                </div>
                            </div>
                        </div>
                    </div>
                </section>
                <!-- ##### Listings Content Area End ##### -->

                <a id="scrollUp" href="#top" style="position: fixed; z-index: 2147483647; display: none;"><i
                        class="fa fa-angle-up" aria-hidden="true"></i></a>

@endsection
