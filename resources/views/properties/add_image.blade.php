@extends('layouts.app')

@section('page_specific_styles')
    <link rel="stylesheet" href="{{ asset("css/pages_css/create_property_page.css") }}">
@endsection

@section('page_specific_scripts_2')
    <script src="{{ asset("js/pages_js/add_image_page.js") }}"></script>
@endsection

@section('app_content')
    <div class="linear_gradient top_margin bottom_margin">
        <div class="container">
            <div class="row justify-content-center" id="create_property_page">
                <div class="col-md-10">
                    <div class="text-center mb-50"><h1>Ajout d'une image à l'annonce</h1></div>

                    @include('layouts.includes.flashMessages')
                    <form id="add_image_form" enctype="multipart/form-data" method="POST"
                          action="{{ route('properties.store_image', $property) }}">
                        @csrf

                        <div id='property_images'>

                            <!-- Autres Images -->
                            <div id="img_others">
                                <div class="row" id='property_img_others'>
                                    <div class="col">
                                        <div class="row">
                                            @for($i = 0; $i < $nbImgs - 1; $i++)
                                                <div
                                                    class="col-md-6 img_input_div {{ $i === 0 ? 'hidden_img_input' : '' }}">
                                                    <div class='img_preview'>
                                                        <label>Cliquez ici pour selectionner une image</label>
                                                    </div>
                                                    <input type="file" name="img_others_files[]" class='img_input'
                                                        {{ $i === 0 ? 'required' : '' }}>
                                                    <span class='invalid-feedback feedback_span' role='alert'></span>
                                                    @if($i === 0)
                                                        <div class="mb-3 ml-2">
                                                            <button class="btn delete_img_input_btn">
                                                                Supprimer
                                                            </button>
                                                        </div>
                                                    @endif
                                                </div>
                                            @endfor
                                        </div>
                                    </div>
                                </div>

                                <div class="row mt-5">
                                    <div class="col">
                                        <button id="add_img_btn" class="btn south-btn south-btn-small">
                                            Ajouter une image
                                        </button>
                                        <p class='invalid-feedback feedback_span' role='alert'>
                                            Vous ne pouvez plus ajouter d'autres images</p>
                                    </div>
                                </div>
                            </div>

                        </div>


                        <div class="row mt-30">
                            <div class="col">
                                <a href="{{ route('properties.edit', $property) }}" class="btn south-btn"
                                   id="cancel_property_create">Annuler</a>
                                <input type="submit" name="add_image_form_submit" id="add_image_form_submit"
                                       class="btn south-btn"
                                       value="Sauvegarder">
                            </div>
                        </div>
                    </form>

                </div>
            </div>
        </div>
    </div>
@endsection
