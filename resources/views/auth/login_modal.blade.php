
<!-- Button trigger modal -->
<li><a href="#"><button type="button" class="btn btn-primary" data-toggle="modal" data-target="#exampleModalCenter">
            Connexion
        </button></a></li>


<!-- Modal -->
<div class="modal fade" id="exampleModalCenter" tabindex="-1" role="dialog"
     aria-labelledby="exampleModalCenterTitle" aria-hidden="true" data-backdrop="static" data-keyboard="false">
    <div class="modal-dialog modal-dialog-centered" role="document">
        <div class="modal-content"style="max-width: 450px;">
            <div class="modal-header">
                <h2 class="fs-title">Connectez-vous</h2>
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true">&times;</span>
                </button>
            </div>
            <div class="modal-body">
                <div class="form-group">
                    <label for="exampleInputEmail1">Email address</label>
                    <input type="email" class="form-control" id="exampleInputEmail1" aria-describedby="emailHelp" placeholder="Enter email">
                </div>
                <div class="form-group">
                    <label for="exampleInputPassword1">Password</label>
                    <input type="password" class="form-control" id="exampleInputPassword1" placeholder="Password">

                </div>
                <div class="form-group">
                    <div class="row">
                        <div class="col">
                            <div class="form-check">
                                <input type="checkbox" class="form-check-input" id="exampleCheck1">
                                <label class="form-check-label" for="exampleCheck1">Rester connecté</label>
                            </div>
                        </div>
                        <div class="col text-right">
                            <a href="#">Mot de passe oublié ?</a>
                        </div>
                    </div>
                </div>
                <div class="text-center">
                    <button type="submit" class="btn btn-primary">Connexion</button>
                </div>
            </div>
            <div class="modal-footer">
                <div class="text-right">
                    <a href="#">Inscrivez-vous</a>
                </div>
            </div>
            </form>
        </div>
    </div>
</div>
