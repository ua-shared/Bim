@extends('layouts.app')

@section('page_specific_scripts_2')
    <script src="{{ asset("js/pages_js/signup_page.js") }}"></script>
@endsection


@section('app_content')
    <div class="linear_gradient top_margin bottom_margin page_min_height" id="signup_page">
        <div class="container">
            <div class="row justify-content-center">
                <div class="col-md-9">

                    <div class="text-center"><h1>Inscription</h1></div>
                    <form id="signup_form" method="POST" action="{{ route('register') }}">
                    @csrf

                    <!-- fieldset 1 -->
                        <fieldset>
                            <legend>Informations de connexion</legend>

                            @include('partials.select', [
                           'title' => "Type de compte",
                           'name' => 'account_type',
                           'required' => true,
                           'selected' => 'personnal',
                           'class' => 'nice_select wide',
                           'options' => [
                           '' => 'Choisissez une option',
                           'personnal' => 'Compte personnel',
                           'enterprise' => 'Compte Entreprise',
                           ],
                           ])

                            @include('partials.input_simple_lines', [
                        'title' => 'Email',
                        'type' => 'email',
                        'name' => 'email',
                        'required' => true,
                        'value' => 'user3@test.bj',
                        ])

                            @include('partials.input_simple_lines', [
                        'title' => "Nom d'utilisateur",
                        'type' => 'text',
                        'name' => 'username',
                        'required' => true,
                        'value' => 'username3',
                        ])

                            @include('partials.input_simple_lines', [
                        'title' => 'Mot de passe',
                        'type' => 'password',
                        'name' => 'password',
                        'required' => true,
                        'value' => 'aaaaaa',
                        ])

                            @include('partials.input_simple_lines', [
                        'title' => 'Confirmez le mot de passe',
                        'type' => 'password',
                        'id' => 'password-confirm',
                        'name' => 'password_confirmation',
                        'required' => true,
                        'value' => 'aaaaaa',
                        'hideErrors',
                        ])

                        </fieldset>

                        <!-- fieldset 2 -->
                        <fieldset>
                            <legend>Informations personnels</legend>


                            @include('partials.input_simple_lines', [
                        'title' => 'Nom',
                        'type' => 'text',
                        'name' => 'name',
                        'required' => true,
                        'value' => 'Nom',
                        ])

                            @include('partials.input_simple_lines', [
                        'title' => 'Prénoms',
                        'type' => 'text',
                        'name' => 'firstname',
                        'required' => true,
                        'value' => 'Prenoms 01 02',
                        ])

                            @include('partials.input_simple_lines', [
                        'title' => 'Téléphone',
                        'type' => 'tel',
                        'name' => 'tel',
                        'required' => true,
                        'value' => '+229 00 00 00 00',
                        'others' => 'pattern="^(\+|\d)[0-9-\s]{7,20}$"',
                        ])

                            @include('partials.textarea', [
                        'title' => 'Adresse',
                        'name' => 'address',
                        'required' => true,
                        'value' => 'Adresse',
                        ])

                        </fieldset>

                        <!-- fieldset 3 -->
                        <fieldset>
                            <legend>Géolocalisation</legend>

                            <p>
                                Vous etes presque à la fin du processus d'inscription.
                                Nous avons besoin de votre localisation GPS afin de vous proposer des offres
                                personnalisées situées dans votre région.


                                @include('partials.checkbox', [
                            'title' => 'Autoriser la localisation',
                            'name' => 'accept_locate',
                            ])

                                @include('partials.checkbox', [
                            'title' => "J'accepte les termes du <a href='#'>contrat d'utilisation</a>.",
                            'name' => 'accept_contract',
                            ])
                            </p>

                            <div class="row">
                                <div class="col">
                                    <p>
                                        <input type="hidden" name="lat" id="lat" value="">
                                        <input type="hidden" name="lng" id="lng" value="">
                                        <span class="invalid-feedback feedback_span" role="alert"></span>
                                    </p>
                                </div>
                            </div>

                            <input type="submit" name="signup_btn" id="signup_btn"
                                   class="btn south-btn my_color_01"
                                   value="S'inscrire"/>
                        </fieldset>
                    </form>
                </div>
            </div>
        </div>
    </div>

@endsection
