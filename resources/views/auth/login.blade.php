@extends('layouts.app')

@section('app_content')
    <div class="top_margin bottom_margin page_min_height" id="login_page">
        <div class="container">
            <div class="row justify-content-center">
                <div class="col-md-6">
                    <div class="card">
                        <div class="card-header text-center"><h3>Connection</h3></div>

                        <div class="card-body">

                            <!-- ##### Login Form Start ##### -->
                            <form method="POST" action="{{ route('login') }}">
                                @csrf

                                @include('partials.input_simple_lines', [
                                'title' => 'Votre email',
                                'type' => 'email',
                                'name' => 'email',
                                'required' => true,
                                ])

                                @include('partials.input_simple_lines', [
                                'title' => 'Mot de passe',
                                'type' => 'password',
                                'name' => 'password',
                                'required' => true,
                                ])

                                <div class="form-group row">
                                    <div class="col-md-6 text-left">
                                        <div class="checkbox">
                                            <label>
                                                <input type="checkbox"
                                                       name="remember" {{ old('remember') ? 'checked' : '' }}> {{ __('Remember Me') }}
                                            </label>
                                        </div>
                                    </div>
                                    <div class="col-md-6 text-right">
                                        <a href="{{ route('password.request') }}">
                                            Mot de passe oublié ?
                                        </a>
                                    </div>
                                </div>

                                <div class="row text-center">
                                    <div class="col">
                                        <input type="submit" class="btn south-btn" value="Se connecter">
                                    </div>
                                </div>
                            </form>
                            <!-- ##### Login Form Start ##### -->

                        </div>
                        <div class="card-footer text-right">
                            Pas de compte ? <a href="{{ route('register') }}">Inscrivez-vous ici</a>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection
