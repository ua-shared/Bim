@extends('layouts.app')

@section('app_content')

    <div id="register_content" class="auth_form">
        <div class="container">
            <div class="row justify-content-center">
                <div class="col-md-9">
                    <!-- multistep form -->
                    <form id="msform" method="POST" action="{{ route('register') }}">
                    @csrf

                    <!-- progressbar -->
                        <ul id="progressbar">
                            <li class="active">Informations de connexion</li>
                            <li>Informations personnels</li>
                            <li>Géolocalisation</li>
                        </ul>
                        <!-- fieldset 1 -->
                        <fieldset>
                            <h2 class="fs-title">Inscription</h2>
                            <h3 class="fs-subtitle">Etape 1</h3>

                            <div class="form-group">
                                <label for="account_type">Type de compte</label>

                                <div class="row">
                                    <div class="col">
                                    <select name="account_type"
                                            id="account_type" class="{{ $errors->has('name') ? ' is-invalid' : '' }}"
                                            name="account_type" value="{{ old('name') }}" required autofocus>
                                        <option value="">--- Choisissez votre type de compte ---</option>
                                        <option value="personnal" {{ old('name') == "personnal" ? "selected" : "" }}>
                                            Compte personnel
                                        </option>
                                        <option value="enterprise" {{ old('name') == "enterprise" ? "selected" : "" }}>
                                            Compte Entreprise
                                        </option>
                                    </select>

                                    @if ($errors->has('name'))
                                        <span class="invalid-feedback" role="alert">
                                        <strong>{{ $errors->first('name') }}</strong>
                                    </span>
                                    @endif
                                </div>
                                </div>
                            </div>

                            @include('partials.input_simple_lines', [
                        'title' => 'Email',
                        'type' => 'email',
                        'name' => 'email',
                        'required' => true,
                        ])

                            @include('partials.input_simple_lines', [
                        'title' => "Nom d'utilisateur",
                        'type' => 'text',
                        'name' => 'username',
                        'required' => true,
                        ])

                            @include('partials.input_simple_lines', [
                        'title' => 'Mot de passe',
                        'type' => 'password',
                        'name' => 'pass',
                        'required' => true,
                        ])

                            @include('partials.input_simple_lines', [
                        'title' => 'Confirmez le mot de passe',
                        'type' => 'password',
                        'name' => 'pass-conf',
                        'required' => true,
                        ])

                            <input type="button" name="next" class="next action-button" value="Suivant"/>
                        </fieldset>

                        <!-- fieldset 2 -->
                        <fieldset>
                            <h2 class="fs-title">Créez un compte</h2>
                            <h3 class="fs-subtitle">Etape 2</h3>


                            @include('partials.input_simple_lines', [
                        'title' => 'Nom',
                        'type' => 'text',
                        'name' => 'name',
                        'required' => true,
                        ])

                            @include('partials.input_simple_lines', [
                        'title' => 'Prénoms',
                        'type' => 'text',
                        'name' => 'firstname',
                        'required' => true,
                        ])

                            @include('partials.input_simple_lines', [
                        'title' => 'Téléphone',
                        'type' => 'tel',
                        'name' => 'tel',
                        'required' => true,
                        ])

                            <div class="form-group">
                                <label for="address">Adresse</label>

                                    <textarea id="address"
                                              class="{{ $errors->has('address') ? ' is-invalid' : '' }}"
                                              name="address" required rows="4">{{ old('address') }}</textarea>

                                    @if ($errors->has('address'))
                                        <span class="invalid-feedback" role="alert">
                                                <strong>{{ $errors->first('address') }}</strong>
                                            </span>
                                    @endif
                            </div>

                            <input type="button" name="previous" class="previous action-button" value="Précedent"/>
                            <input type="button" name="next" class="next action-button" value="Suivant"/>
                        </fieldset>

                        <!-- fieldset 3 -->
                        <fieldset>
                            <h2 class="fs-title">Créez un compte</h2>
                            <h3 class="fs-subtitle">Etape 3</h3>

                            <p>
                                Vous etes presque à la fin du processus d'inscription.
                            </p>
                            <p>
                                Nous avons besoin de votre localisation GPS afin de vous proposer des offres
                                personnalisées situées dans votre région.
                                <div class="checkbox">
                                    <label><input type="checkbox" id="accept_locate"
                                                  name="accept_locate" {{ old('remember') ? 'checked' : '' }}> Autoriser la localisation
                                        </label>
                                </div>
                            </p>

                            <input type="hidden" name="longitude" value="">
                            <input type="hidden" name="latitude" value="">

                            <input type="button" name="previous" class="previous action-button" value="Précedent"/>
                            <input type="button" name="next" class="next action-button my_color_01" style="background-color: #fd9644;" value="S'inscrire"/>
                        </fieldset>
                    </form>

                </div>
            </div>
        </div>
    </div>

@endsection
